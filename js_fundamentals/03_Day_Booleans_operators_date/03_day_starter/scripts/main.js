/*1. Create a human readable time format using the Date time object
YYYY-MM-DD HH:mm
DD-MM-YYYY HH:mm
DD/MM/YYYY HH:mm
 */
function getDateInFormat(prop, dateFormat) {
  const date = new Date(prop);
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  if (dateFormat === "YYYY-MM-DD") {
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
  }
  if (dateFormat === "DD-MM-YYYY") {
    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
  }
  if (dateFormat === "DD/MM/YYYY") {
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
  }
  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}
