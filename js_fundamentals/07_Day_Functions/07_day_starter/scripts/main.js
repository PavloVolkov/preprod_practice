// 1 Write a function name showDateTime which shows time in this format: 08/01/2020 04:08 using the Date object
function showDateTime(dateToDisplay) {
  const date = new Date(dateToDisplay);
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hours = date.getHours();
  const minutes = date.getMinutes();

  const monthFormated = month < 10 ? `0${month}` : month;
  const dayFormated = day < 10 ? `0${day}` : day;
  const hoursFormated = hours < 10 ? `0${hours}` : hours;
  const minutesFormated = minutes < 10 ? `0${minutes}` : minutes;

  console.log(`${monthFormated}/${dayFormated}/${year} ${hoursFormated}:${minutesFormated}`);
}

// 2 Declare a function name swapValues. This function swaps value of x to y (without using extra variables)
function swapValues([x, y]) {
  return [y, x];
}
// 3 Declare a function name reverseArray. It takes array as a parameter and it returns the reverse of the array (don't use method)
function reverseArray(arr) {
  const reversedArr = [];
  for (let i = 0; i < arr.length; i++) {
    reversedArr.unshift(arr[i]);
  }
  return reversedArr;
}
// 4 Write a function which takes any number of arguments and return the sum of the arguments
function sum() {
  return Array.from(arguments).reduce((acc, el) => {
    return acc + el;
  });
}
// 5 Call your function shuffleArray, it takes an array as a parameter and it returns a shuffled array
function shuffleArray(arr) {
  return arr.sort((a, b) => 0.5 - Math.random());
}
// 6 Call your function factorial, it takes a whole number as a parameter and it return a factorial of the number

function factorial(n) {
  return n != 1 ? n * factorial(n - 1) : 1;
}
// 7 Write a functions which checks if all items are unique in the array
//Write a functions which checks if all items are unique in the array
function eachUnique(arr) {
  return arr.length === new Set(arr).size; // чесно знайдено на стековерфлоу 'You compare length of your array and size of the Set which always contains uniq entries.'
}
// 8 Write a function which checks if all
//the items of the array are the same data type
function isMonoDataType(arr) {
  return arr.every(e => typeof e === typeof arr[0]);
}
