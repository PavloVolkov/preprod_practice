import { countries } from "../../../countries_data.js";
const webTechs = [
  "HTML",
  "CSS",
  "JavaScript",
  "React",
  "Redux",
  "Node",
  "MongoDB",
];

const frontEnd = ["HTML", "CSS", "JS", "React", "Redux"];
const backEnd = ["Node", "Express", "MongoDB"];
const ages = [19, 22, 19, 24, 20, 25, 26, 24, 25, 24];
// 1 First remove all the punctuations and change the string to array and count the number of words in the array
let text =
  "I love teaching and empowering people. I teach HTML, CSS, JS, React, Python.";
text = text.replace(/[.\,]/g, "").split(" ");

// 2 In countries array check if 'Ethiopia' exists in the array if it exists print 'ETHIOPIA'. If it does not exist add to the countries list and print the array.
if (countries.includes("Ethiopia")) {
  console.log("ETHIOPIA");
} else {
  countries.push("Ethiopia");
  console.log(countries);
}

// 3 In the webTechs array check if Sass exists in the
//array and if it exists print 'Sass is a CSS preprocess'. If it does not exist add Sass to the array and print the array.

if (webTechs.includes("Sass")) {
  console.log("Sass is a CSS preprocess");
} else {
  webTechs.push("Sass");
  console.log(webTechs);
}

// 4 Concatenate the following two variables and store it in a fullStack variable.
const fullStack = frontEnd.concat(backEnd);

// 5 The following is an array of 10 students ages:
function sortArr(prop) {
  return Array.isArray(prop) ? prop.sort((a, b) => a - b) : [];
}

function minAge(prop) {
  let sorted = Array.isArray(prop) ? prop.sort((a, b) => a - b) : [];
  return sorted[0];
}

function maxAge(prop) {
  let sorted = Array.isArray(prop) ? prop.sort((a, b) => a - b) : [];
  return sorted[sorted.length - 1];
}

function getMedian(prop) {
  let sorted = Array.isArray(prop) ? prop.sort((a, b) => a - b) : [];
  return sorted.length % 2
    ? sorted[Math.round(sorted.length / 2)]
    : (sorted[Math.round(sorted.length / 2)] +
        sorted[Math.round(sorted.length / 2 + 1)]) /
        2;
}

function getAverage(prop) {
  const sorted = Array.isArray(prop) ? prop.sort((a, b) => a - b) : [];
  const sum = sorted.reduce((acc, el) => {
    return acc + el;
  });
  return sum / prop.length;
}

function rangeOfAges(prop) {
  return maxAge(prop) - minAge(prop);
}

function compare(prop) {
  return Math.abs(minAge(prop) - getAverage(prop)) >
    Math.abs(maxAge(prop) - getAverage(prop))
    ? Math.abs(minAge(prop) - getAverage(prop))
    : Math.abs(maxAge(prop) - getAverage(prop));
}
