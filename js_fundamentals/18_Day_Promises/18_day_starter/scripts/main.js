const countriesAPI = 'https://restcountries.com/v3.1/all';
const catsAPI = 'https://api.thecatapi.com/v1/breeds';
// 1. Read the countries API using fetch
//and print the name of country, capital,
//languages, population and area

async function getCountries() {
  const result = await fetch(countriesAPI, {
    method: 'GET',
    headers: {},
  });
  const json = await result.json();
  return json.map(({ name, capital, languages, population, area }) => {
    return {
      name,
      capital,
      languages,
      population,
      area,
    };
  });
}

const ptrintCountries = getCountries().then(console.log);

//2. Print out all the cat names in to catNames

async function catsFetch() {
  const result = await fetch(catsAPI, {
    method: 'GET',
    headers: {},
  });
  const json = await result.json();
  return json.map(({ name }) => {
    return {
      name,
    };
  });
}
const ptrintCatNames = catsFetch().then(console.log);

// 3. Read the countries api and find out the 10 largest countries
async function tenLargest(promise, compareFnc, sortFnc) {
  return promise.then(e => sortFnc(e, compareFnc)).then(el => el.slice(0, 10));
}

const tenLargestCountries = tenLargest(getCountries(), compareByAreaFromBigToLow, sortArr).then(console.log);

function sortArr(arr, compare) {
  return Array.isArray(arr) ? arr.sort(compare) : [];
}

function compareByAreaFromBigToLow(a, b) {
  if (a.area > b.area) {
    return -1;
  }
  if (a.area < b.area) {
    return 1;
  }
  return 0;
}

// 4.  Read the countries api and count total number of languages
// in the world used as officials
async function lotalCountOfLanguages(promise) {
  // console.log(promise);
  return promise
    .then(e =>
      e.reduce((acc, { languages }) => {
        let languagePack = languages ? Object.values(languages) : '';
        languagePack.length !== 0 ? acc.push(...languagePack) : '';
        return acc;
      }, []),
    )
    .then(e => new Set(e).size);
}

const languages = lotalCountOfLanguages(getCountries()).then(console.log);
