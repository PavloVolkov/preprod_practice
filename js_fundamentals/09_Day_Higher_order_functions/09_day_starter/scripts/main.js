import { countries } from '../../../countries_data.js';

const products = [
  { product: 'banana', price: 3 },
  { product: 'mango', price: 6 },
  { product: 'potato', price: '  ' },
  { product: 'avocado', price: 8 },
  { product: 'coffee', price: 10 },
  { product: 'tea', price: '  012' },
];
// 1. Find the total price of products by chaining two or more array
//iterators(eg. arr.map(callback).filter(callback).reduce(callback))

function totalPrice(arr) {
  return arr
    .filter(e => typeof e.price === 'number')
    .reduce((acc, e) => {
      return acc + e.price;
    }, 0);
}

// 2. Find the sum of price of products using only reduce reduce(callback))

function totalPriceOnlyReduce(arr) {
  arr = arr.filter(e => typeof e.price === 'number');
  return arr.reduce((acc, { price }) => {
    return acc + price;
  }, 0);
}

// 3. Declare a  function and return an array of ten countries.
// Use different functional programming to work on the countries.js array
function getFirstTenCountries(arr) {
  return arr.slice(0, 10);
}
// 4. Declare a getLastTenCountries function which returns the last ten countries
//in the countries array
function getLastTenCountries(arr, frstTen, revfnc) {
  let lastTen = frstTen(revfnc(arr));
  return lastTen;
}
//console.log(getLastTenCountries(countries, getFirstTenCountries, reverseArray));

function reverseArray(arr) {
  return arr.map((e, index, array) => array[array.length - 1 - index]);
}

// 5. Use the countries information, in the data folder.
//Sort countries by name, by capital, by population // return array with defined property

const sortCountriesByCapital = countries.map(({ capital }) => capital).sort();
const sortCountriesByName = countries.map(({ name }) => name).sort();
const sortByPopulation = countries.map(({ population }) => population).sort(compareByPopulationFrombigToLow);

function compareByPopulationFrombigToLow(a, b) {
  if (a.population > b.population) {
    return -1;
  }
  if (a.population < b.population) {
    return 1;
  }
  return 0;
}
// 6. *** Find the 10 most spoken languages:
function tenMostSpokenLang(arr) {
  // needs to save each language separtely
  let countedLangObj = {}; // to save how much each language repeats

  let languagesArr = arr.map(el => {
    return el.languages;
  });

  languagesArr.forEach(el => {
    countedLangObj.hasOwnProperty(el) ? (countedLangObj[el] += 1) : (countedLangObj[el] = 1); // step to
  }); // calculate how often of each language mention

  languagesArr = Object.entries(countedLangObj); // rewrite languagesArr like lang: count

  let sortedLanguages = languagesArr.sort((a, b) => b[1] - a[1]); // sorting from bigger to lower

  const firstTenMostSpoken = getFirstTenCountries(sortedLanguages); // get first ten lang

  return firstTenMostSpoken;
}
console.log(tenMostSpokenLang(countries));

// 7. *** Use countries_data.js file create a function which create the ten most populated countries
function tenMostPopulatedCountries(arr, comparator, getfirstTen) {
  return getfirstTen(arr.sort(comparator));
}
//console.log(tenMostPopulatedCountries(countries, compareByPopulationFrombigToLow, getFirstTenCountries));
