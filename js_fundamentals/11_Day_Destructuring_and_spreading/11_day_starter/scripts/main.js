import { countries } from '../../../countries_data.js';
const student = {
  name: 'David',
  age: 25,
  skills: {
    frontEnd: [
      { skill: 'HTML', level: 10 },
      { skill: 'CSS', level: 8 },
      { skill: 'JS', level: 8 },
      { skill: 'React', level: 9 },
    ],
    backEnd: [
      { skill: 'Node', level: 7 },
      { skill: 'GraphQL', level: 8 },
    ],
    dataBase: [{ skill: 'MongoDB', level: 7.5 }],
    dataScience: ['Python', 'R', 'D3.js'],
  },
};
const students = [
  ['David', ['HTM', 'CSS', 'JS', 'React'], [98, 85, 90, 95]],
  ['John', ['HTM', 'CSS', 'JS', 'React'], [85, 80, 85, 80]],
];
const users = [
  {
    name: 'Brook',
    scores: 75,
    skills: ['HTM', 'CSS', 'JS'],
    age: 16,
  },
  {
    name: 'Alex',
    scores: 80,
    skills: ['HTM', 'CSS', 'JS'],
    age: 18,
  },
  {
    name: 'David',
    scores: 75,
    skills: ['HTM', 'CSS'],
    age: 22,
  },
  {
    name: 'John',
    scores: 85,
    skills: ['HTML'],
    age: 25,
  },
  {
    name: 'Sara',
    scores: 95,
    skills: ['HTM', 'CSS', 'JS'],
    age: 26,
  },
  {
    name: 'Martha',
    scores: 80,
    skills: ['HTM', 'CSS', 'JS'],
    age: 18,
  },
  {
    name: 'Thomas',
    scores: 90,
    skills: ['HTM', 'CSS', 'JS'],
    age: 20,
  },
];
// 1. Iterate through the users array and get all the keys
// of the object using destructuring
function getKeys(arr) {
  return arr.forEach(({ name, scores, skills, age }) => {
    console.log(name, scores, skills, age);
  });
}
// 2. Find the persons who have less than two skills
function lessThanTwoSkills(arr) {
  return arr.filter(({ skills }) => skills.length < 2);
}
// 3. Destructure the countries object print name, capital, population and languages of all countries
function printCountryInfo(arr) {
  arr.forEach(({ capital, name, population, languages }) =>
    console.log(`Country - ${name} with capital ${capital}, country population is ${population},
there are following languages in use: ${[...languages]}`),
  );
}
// 4. Write a function called convertArrayToObject which can convert the array to a structure object.

function convertArrayToObject(arr) {
  return arr.map(([name, skills, scores]) => {
    return {
      name: name,
      skills: skills,
      scores: scores,
    };
  });
}
/** 5Copy the student object to newStudent without 
mutating the original object. 
In the new object add the following:
Add Bootstrap with level 8 to the front end skill sets
Add Express with level 9 to the back end skill sets
Add SQL with level 8 to the data base skill sets
Add SQL without level to the data science skill sets*/
function copyObject(obj) {
  return {
    ...obj,
    skills: {
      frontEnd: [
        { skill: 'HTML', level: 10 },
        { skill: 'CSS', level: 8 },
        { skill: 'JS', level: 8 },
        { skill: 'React', level: 9 },
      ],
      backEnd: [
        { skill: 'Node', level: 7 },
        { skill: 'GraphQL', level: 8 },
      ],
      dataBase: [{ skill: 'MongoDB', level: 7.5 }],
      dataScience: ['Python', 'R', 'D3.js'],
    },
  };
}

const newStundent = copyObject(student); // solution const newStudent = { ...student };
newStundent.skills.frontEnd.push({ skill: 'Bootstrap', level: 8 });
newStundent.skills.backEnd.push({ skill: 'Express', level: 9 });
newStundent.skills.dataBase.push({ skill: 'SQL', level: 8 });
newStundent.skills.dataScience.push('SQL');
