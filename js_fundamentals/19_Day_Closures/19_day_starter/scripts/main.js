// Create a personAccount out function.
// It has firstname, lastname, incomes, expenses  inner variables.
// It has totalIncome, totalExpense, accountInfo, addIncome,
// addExpense and accountBalance inner functions.
//  Incomes is a set of incomes and its description and expenses is
// also a set of expenses and its description.

function personAccount({ firstname, lastname, incomes = [], expenses = [] }) {
  // Create a personAccount out function. It has firstname, lastname, incomes, expenses  inner variables.
  function addIncome(desc, inc) {
    return incomes.push([desc, inc]);
  } // It has addIncome,
  function addExpense(exp, desc) {
    return expenses.push([desc, exp]);
  } // It has addExpense,
  function totalExpense() {
    return expenses; //
  } // It has totalExpense,
  function totalIncome() {
    return incomes;
  } // It has totalIncome,
  function accountBalance() {
    const incomesSum = incomes.reduce((acc, e) => {
      return acc + e[1];
    }, 0);

    const expensesSum = expenses.reduce((acc, e) => {
      return acc + e[1];
    }, 0);

    return incomesSum - expensesSum;
  } // It has accountBalance
  function accountInfo() {
    return console.log(firstname, lastname);
  } //  It has accountInfo,
  return {
    addIncome: addIncome,
    addExpense: addExpense,
    totalExpense: totalExpense,
    totalIncome: totalIncome,
    accountBalance: accountBalance,
    accountInfo: accountInfo,
  };
}
const props = {
  firstname: 'Pavlo',
  lastname: 'Volkov',
  incomes: [
    ['desc', 10],
    ['desc2', 15],
    ['desc3', 45],
    ['desc4', 65],
  ],
  expenses: [
    ['desc', 10],
    ['desc2', 15],
    ['desc3', 45],
    ['desc4', 45],
  ],
};

const new1 = personAccount(props);
// // console.log(new1.accountInfo());
// // console.log(new1.totalIncome());
// // console.log(new1.totalIncome()); //['desc4', 65]
// console.log(new1.totalExpense()); //
new1.addIncome('desc12414', 1000);
console.log(new1.accountBalance());
