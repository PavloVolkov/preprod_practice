// 1 Write a code which  can give grades to students according to theirs scores:
//90-100, A
//70-89, B
//60-69, C
//50-59, D
//0-49, F
function grade(score) {
  let gradeOfStudent;
  if (score < 0 || score > 100) {
    gradeOfStudent = 0;
  }
  if (score >= 90 && score <= 100) {
    gradeOfStudent = "A";
  }
  if (score >= 70 && score <= 89) {
    gradeOfStudent = "B";
  }
  if (score >= 60 && score <= 69) {
    gradeOfStudent = "C";
  }
  if (score >= 50 && score <= 59) {
    gradeOfStudent = "D";
  }
  if (score >= 0 && score <= 49) {
    gradeOfStudent = "F";
  }
  return gradeOfStudent;
}
// 2 Check if the season is Autumn, Winter, Spring or Summer.
//If the user input is:
//September, October or November, the season is Autumn.
//December, January or February, the season is Winter.
//March, April or May, the season is Spring
//June, July or August, the season is Summer
function daysCheck(month) {
  month = month.toLowerCase();
  let days;
  switch (month) {
    case ["september", "october", "november"].find((e) => e === month):
      days = "Autumn";
      break;
    case ["december", "january", "february"].find((e) => e === month):
      days = "Winter";
      break;
    case ["march", "april", "may"].find((e) => e === month):
      days = "Spring";
      break;
    case ["june", "july", "august"].find((e) => e === month):
      days = "Summer";
      break;
    default:
      days = `${month} not a month or not a string or both. Repeat with valid value to check what days is.`;
      break;
  }
  return days;
}
// 3 Check if a day is weekend day or a working day. Your script will take day as an input.
function whatIsTheDay() {
  let today = prompt("What is the day today?");
  today = today.toLowerCase();
  switch (today) {
    case ["sunday", "saturday"].find((e) => e === today):
      today = `${today} is a weekend.`;
      break;
    case ["monday", "tuesday", "wednesday", "thursday", "friday"].find(
      (e) => e === today
    ):
      today = `${today} is a working day.`;
      break;
    default:
      today = `${today} not a day of the week or not a string or both. Repeat with valid value to check what day is.`;
      break;
  }
  return today;
}

// 4 Write a program which tells the number of days in a month.
function daysInMonth(month) {
  let days;
  month = month.trim().toLowerCase();
  switch (month) {
    case ["september", "november", "april", "june"].find((e) => e === month):
      days = `${month} has 30 days.`;
      break;
    case [
      "october",
      "december",
      "january",
      "march",
      "may",
      "july",
      "august",
    ].find((e) => e === month):
      days = `${month} has 31 days.`;
      break;
    case "february":
      days = `February has 28 days.`;
      break;
    default:
      days = `${month} not a month or not a string or both. Repeat with valid value to check how many days in a month.`;
      break;
  }
  return days;
}
