import { countries } from "../../../countries_data.js";
const webTechs = [
  "HTML",
  "CSS",
  "JavaScript",
  "React",
  "Redux",
  "Node",
  "MongoDB",
];
// 1 Using the countries array, create an array for countries length'
function getItemsLength(arr) {
  const getItemsLength = [];
  for (let i = 0; i < arr.length; i++) {
    getItemsLength.push(arr[i].length);
  }
  return getItemsLength;
}
// 2 Using the countries array, find the country containing only 5 characters
function arrOfFiveCharsWords(arr) {
  const arrOfFiveCharsWords = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].length === 5) {
      arrOfFiveCharsWords.push(arr[i]);
    }
  }
  console.log(arrOfFiveCharsWords);
}

// 3 Find the longest word in the webTechs array
function getLongestWord(arr) {
  let longestWord = "";
  for (let i = 0; i < arr.length; i++) {
    if (longestWord.length < arr[i].length) {
      longestWord = arr[i];
    }
  }
  return longestWord;
}
// 4 Use the webTechs array to create the following array of arrays
function twoLevelArr(arr) {
  let newArr = [];
  for (let i = 0; i < arr.length; i++) {
    newArr.push([arr[i], arr[i].length]);
  }
  return newArr;
}
// 5 This is a fruit array , ['banana', 'orange', 'mango', 'lemon'] reverse the order using loop without using a reverse method
function reverseArr(arr) {
  const reversedArr = [];
  for (let i = 0; i < arr.length; i++) {
    reversedArr.unshift(arr[i]);
  }
  return reversedArr;
}
// 6  Copy countries array (avoid mutation)
function copyArr(arr) {
  const copyOfArr = [];
  for (let i = 0; i < arr.length; i++) {
    copyOfArr.push(arr[i]);
  }
  return copyOfArr;
}

// 7 Sort the webTechs array

function sortArrABC(words) {
  for (let index = 0; index < words.length - 1; index++) {
    for (let i = 0; i < words.length; i++) {
      let a = words[i];
      let b = words[i + 1];
      if (a > b) {
        words[i] = b;
        words[i + 1] = a;
      }
    }
  }
  return words;
}

// ***
// 1 Find the country containing the hightest number of characters in the countries array
const longestInArr = getLongestWord(countries);
// 2 Extract all the countries contain the word 'land' from the countries array and print it as array
function findIncludedInArray(arr, needToInclude) {
  let includeLand = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].includes(needToInclude)) {
      includeLand.push(arr[i]);
    }
  }
  return includeLand;
}
// 3 Extract all the countries containing two or more words from the countries array and print it as array
function extract(arr, countOfWord) {
  let extracted = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].split(" ").length >= countOfWord) {
      extracted.push(arr[i]);
    }
  }
  return extracted;
}
// 4 Reverse the countries array and capitalize each country and stored it as an array
function reversePlusCapitlize(reverceFnc, arrToRev) {
  let revercedArr = reverceFnc(arrToRev);
  let capitlizedArr = [];
  for (let i = 0; i < revercedArr.length; i++) {
    capitlizedArr.push(capitalize(revercedArr[i]));
  }
  return capitlizedArr;
}

console.log(reversePlusCapitlize(reverseArr, ["a", "b", "c"]));

function capitalize(prop) {
  const strArr = prop.toLowerCase();
  return strArr.replace(/(\w+)/g, (match) => {
    const firstLetterAppCase = match.toLowerCase()[0].toUpperCase();
    const res = match.split("");
    res.splice(0, 1, firstLetterAppCase);
    return res.join("");
  });
}
