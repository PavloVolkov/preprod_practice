// Exercises
// 1 Use substr to slice out the phrase because because because
//from the following sentence:
//'You cannot end a sentence with because because because is a conjunction'
let sentence = 'You cannot end a sentence with because because because is a conjunction';
sentence = sentence.substr(0, 30) + sentence.substr(54, sentence.length - 1);

// 2 'Love is the best thing in this world. Some found their love and some are still
//looking for their love.' Count the number of word love in this sentence.
const loveQuantity =
  'Love is the best thing in this world. Some found their love and some are still looking for their love.'.match(
    /love/gi,
  ).length;

// 3 Use match() to count the number of all because in the following sentence:
//("You cannot end a sentence with because because because is a conjunction");
const becauseQuantity = 'You cannot end a sentence with because because because is a conjunction'.match(
  /because/gi,
).length;

// 4 Calculate the total annual income of the person by
//extracting the numbers from the following text.
// 'He earns 5000 euro from salary per month,
//10000 euro annual bonus, 15000 euro online courses per month.'
const income = 'He earns 5000 euro from salary per month, 10000 euro annual bonus, 15000 euro online courses per month.'
  .match(/\d+/g)
  .reduce((acc, el) => acc + parseInt(el), 0);

//5 Clean the following text and find the most frequent word (hint, use replace and regular expressions).
const sentenceToClean =
  '%I $am@% a %tea@cher%, &and& I lo%#ve %te@a@ching%;. \
 The@re $ is no@th@ing; no@th@ing a &as& mo@re rewarding as educa@ting &and& @emp%o@weri@ng peo@ple.\
  ;I found tea@ching m%o@re interesting tha@n any ot#her %jo@bs. %Do@es thi%s mo@tiv#ate yo@u to be a tea@cher!? \
  %Th#is 30#Days&OfJavaScript &is al@so $the $resu@lt of &love& of tea&ching';

let cleanSentence = sentenceToClean.replace(/[\%,\$,\&,\@,\;,\#]/g, '');
const cleanSentenceArr = cleanSentence.match(/\w+/g);
let repeatCount = 0;
let frequent = '';

for (const word of cleanSentenceArr) {
  const regExp = new RegExp(word, 'g');
  let elWordRepeatCount = [...cleanSentence.matchAll(regExp)].length;
  if (repeatCount < elWordRepeatCount) {
    repeatCount = elWordRepeatCount;
    frequent = `Word ${word}, has ${elWordRepeatCount} occurrences`;
  }
}
