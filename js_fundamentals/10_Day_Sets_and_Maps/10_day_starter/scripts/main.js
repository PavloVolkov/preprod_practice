import {countries} from './countries_data.js';

const a = [4, 5, 8, 9];
const b = [3, 4, 5, 7];
const countries1 = ['Finland', 'Sweden', 'Norway'];

// 1. Find a union b
const aub = new Set([...a, ...b]);
// console.log(aub)

// 2. Find a intersection b

const inters = a.filter(el=> new Set(b).has(el));
// console.log(inters)

// 3. Find a with b / [ 8, 9 ]
const awb = a.filter(el=> !new Set(b).has(el));
// console.log(awb)

// 4.How many languages are there in the countries object file.
function getCountOflang(arr) {
    let languagesArr = [];    // needs to save each language separtely
    arr.forEach(el => {       // this step save each language separtely
        languagesArr = languagesArr.concat(el.languages) // by concat each language array
    });
    return new Set(languagesArr) //set of unique lang 
}
// console.log(getCountOflang(countries))