// import { countries } from '../../../countries_data.js';
const student = {
  firstName: 'Asabeneh',
  lastName: 'Yetayehe',
  age: 250,
  isMarried: true,
  skills: ['HTML', 'CSS', 'JS', 'React', 'Node', 'Python'],
};

const txt = `{
      "Alex": {
          "email": "alex@alex.com",
          "skills": [
              "HTML",
              "CSS",
              "JavaScript"
          ],
          "age": 20,
          "isLoggedIn": false,
          "points": 30
      },
      "Asab": {
          "email": "asab@asab.com",
          "skills": [
              "HTML",
              "CSS",
              "JavaScript",
              "Redux",
              "MongoDB",
              "Express",
              "React",
              "Node"
          ],
          "age": 25,
          "isLoggedIn": false,
          "points": 50
      },
      "Brook": {
          "email": "daniel@daniel.com",
          "skills": [
              "HTML",
              "CSS",
              "JavaScript",
              "React"
          ],
          "age": 30,
          "isLoggedIn": true,
          "points": 50
      },
      "Daniel": {
          "email": "daniel@alex.com",
          "skills": [
              "HTML",
              "CSS",
              "JavaScript",
              "Python"
          ],
          "age": 20,
          "isLoggedIn": false,
          "points": 40
      },
      "John": {
          "email": "john@john.com",
          "skills": [
              "HTML",
              "CSS",
              "JavaScript",
              "React",
              "Redux",
              "Node.js"
          ],
          "age": 20,
          "isLoggedIn": true,
          "points": 50
      },
      "Thomas": {
          "email": "thomas@thomas.com",
          "skills": [
              "HTML",
              "CSS",
              "JavaScript",
              "React"
          ],
          "age": 20,
          "isLoggedIn": false,
          "points": 40
      },
      "Paul": {
          "email": "paul@paul.com",
          "skills": [
              "HTML",
              "CSS",
              "JavaScript",
              "MongoDB",
              "Express",
              "React",
              "Node"
          ],
          "age": 20,
          "isLoggedIn": false,
          "points": 40
      }
  }
  `;
//1.Stringify the students object
//with only firstName, lastName and skills properties
const userStr = JSON.stringify(student, ['firstName', 'lastName', 'skills'], 3);

//2.Parse the txt JSON to object
const parseTxt = JSON.parse(txt, undefined, 4);

//3.Find the user who has many skills from the variable stored in txt
function longestSkillList(text) {
  let parsedObj = JSON.parse(text, undefined, 4);
  const hasMoreSkills = Object.keys(parsedObj).sort((a, b) => parsedObj[b].skills.length - parsedObj[a].skills.length);
  return hasMoreSkills[0];
}
longestSkillList(txt);
