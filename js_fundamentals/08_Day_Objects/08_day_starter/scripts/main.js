const additionalUsers = [
  {
    _id: 'ab12ex',
    username: 'Alex',
    email: 'alex@alex.com',
    password: '123123',
    createdAt: '08/01/2020 9:00 AM',
    isLoggedIn: false,
  },
  {
    _id: 'fg12cy',
    username: 'Asab',
    email: 'asab@asab.com',
    password: '123456',
    createdAt: '08/01/2020 9:30 AM',
    isLoggedIn: true,
  },
  {
    _id: 'zwf8md',
    username: 'Brook',
    email: 'brook@brook.com',
    password: '123111',
    createdAt: '08/01/2020 9:45 AM',
    isLoggedIn: true,
  },
  {
    _id: 'eefamr',
    username: 'Martha',
    email: 'martha@martha.com',
    password: '123222',
    createdAt: '08/01/2020 9:50 AM',
    isLoggedIn: false,
  },
  {
    _id: 'ghderc',
    username: 'Thomas',
    email: 'thomas@thomas.com',
    password: '123333',
    createdAt: '08/01/2020 10:00 AM',
    isLoggedIn: false,
  },
];

const products = [
  {
    _id: 'eedfcf',
    name: 'mobile phone',
    description: 'Huawei Honor',
    price: 200,
    ratings: [
      { userId: 'fg12cy', rate: 5 },
      { userId: 'zwf8md', rate: 4.5 },
    ],
    likes: [],
  },
  {
    _id: 'aegfal',
    name: 'Laptop',
    description: 'MacPro: System Darwin',
    price: 2500,
    ratings: [],
    likes: ['fg12cy'],
  },
  {
    _id: 'hedfcg',
    name: 'TV',
    description: 'Smart TV:Procaster',
    price: 400,
    ratings: [{ userId: 'fg12cy', rate: 5 }],
    likes: ['fg12cy'],
  },
];

const users = {
  Alex: {
    email: 'alex@alex.com',
    skills: ['HTML', 'CSS', 'JavaScript'],
    age: 20,
    isLoggedIn: false,
    points: 30,
  },
  Brook: {
    email: 'daniel@daniel.com',
    skills: ['HTML', 'CSS', 'JavaScript', 'React', 'Redux'],
    age: 30,
    isLoggedIn: true,
    points: 50,
  },
  Daniel: {
    email: 'daniel@alex.com',
    skills: ['HTML', 'CSS', 'JavaScript', 'Python'],
    age: 20,
    isLoggedIn: false,
    points: 40,
  },
  Asab: {
    email: 'asab@asab.com',
    skills: ['HTML', 'CSS', 'JavaScript', 'Redux', 'MongoDB', 'Express', 'React', 'Node'],
    age: 25,
    isLoggedIn: false,
    points: 50,
  },
  John: {
    email: 'john@john.com',
    skills: ['HTML', 'CSS', 'JavaScript', 'React', 'Redux', 'Node.js'],
    age: 20,
    isLoggedIn: true,
    points: 50,
  },
  Thomas: {
    email: 'thomas@thomas.com',
    skills: ['HTML', 'CSS', 'JavaScript', 'React'],
    age: 20,
    isLoggedIn: false,
    points: 40,
  },
  Paul: {
    email: 'paul@paul.com',
    skills: ['HTML', 'CSS', 'JavaScript', 'MongoDB', 'Express', 'React', 'Node'],
    age: 20,
    isLoggedIn: false,
    points: 40,
  },
};

// 1 Find the person who has many skills in the users object

function getUserWith(obj) {
  const biggestSkillListName = Object.keys(obj).sort((a, b) => obj[b].skills.length - obj[a].skills.length)[0];
  const biggestSkillList = obj[biggestSkillListName].skills.join(', ');
  return `${biggestSkillListName} has skills ${biggestSkillList}`;
}

// 2 Count logged in users, count users having greater than equal to
//50 points from the following object
function loggedUsers(obj, loginPoints) {
  let isLogged = 0;
  let greaterThan = 0;
  for (const key in obj) {
    const element = obj[key];
    if (element.isLoggedIn) {
      isLogged++;
    }
    if (element.points >= loginPoints) {
      greaterThan++;
    }
  }

  return `The number of logged in users - ${isLogged}
The number of users with greater than or equal to 50 points - ${greaterThan}`;
}
/*
 * 3 Create an object literal called personAccount. It has firstName,
 * lastName, incomes, expenses properties and it has totalIncome,
 * totalExpense, accountInfo,addIncome, addExpense and accountBalance
 *  methods. Incomes is a set of incomes and its description and expenses
 * is a set of incomes and its description.
 */
const personAccount = {
  firstName: 'John',
  lastName: 'Doe',
  incomes: [],
  expenses: [],

  totalIncome() {
    return this.incomes.reduce((acc, el) => {
      return acc + el.addedIncome;
    }, 0);
  },

  totalExpense() {
    return this.expenses.reduce((acc, el) => {
      return acc + el.addedExpense;
    }, 0);
  },

  accountInfo() {
    return `${this.firstName} ${this.lastName} `;
  },

  addIncome(inc, descr) {
    const newIncome = {
      addedIncome: inc,
      description: descr,
    };
    return this.incomes.push(newIncome);
  },

  addExpense: function (exp, descr) {
    const newExpense = {
      addedExpense: exp,
      description: descr,
    };
    return this.expenses.push(newExpense);
  },

  accountBalance: function () {
    return this.totalIncome() - this.totalExpense();
  },
};

// 1 a Create a function called signUp which allows user to add to the collection.
// If user exists, inform the user that he has already an account.

function signUp(name, email, pass, userBase, fncToCheckUser) {
  const userExists = fncToCheckUser(email, userBase);
  if (userExists.userExist) {
    const prof = userExists.userProfile;
    console.log(`The user is already registered - `, prof);
  } else {
    const newUser = {
      _id: `${Math.floor(Math.random() * 100 * Math.random())}`,
      username: name,
      email: email,
      password: pass,
      createdAt: `${nowTime()}`,
      isLoggedIn: false,
    };
    userBase.push(newUser);
    console.log(`User registered - `, newUser);
  }
}

function userExists(email, userbase) {
  for (const key in userbase) {
    if (key in userbase) {
      const element = userbase[key];
      if (element.email === email) {
        return { userExist: true, userProfile: element };
      }
    }
  }
  return false;
}

function nowTime() {
  // date format YYYY-MM-DD || DD-MM-YYYY || DD/MM/YYYY
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hours = date.getHours();
  const minutes = date.getMinutes();

  const monthFormated = month < 10 ? `0${month}` : month;
  const dayFormated = day < 10 ? `0${day}` : day;
  const hoursFormated = hours < 10 ? `0${hours}` : hours;
  const minutesFormated = minutes < 10 ? `0${minutes}` : minutes;

  return `${monthFormated}/${dayFormated}/${year} ${hoursFormated}:${minutesFormated}`;
}
// 1 b Create a function called signIn which allows user to sign in to the application

function signIn(email, password, userbase) {
  const user = userExists(email, userbase);

  if (user && user.userProfile.password === password) {
    if (user.userProfile.isLoggedIn) {
      console.log('user is already logged in');
    } else {
      user.userProfile.isLoggedIn = true;
      console.log('user is logged in');
    }
  } else {
    console.log('user email or password are incorrect');
  }
}

/**
 * 1 The products array has three elements and each of them has six properties.
a. Create a function called rateProduct which rates the product
b. Create a function called averageRating which calculate the average rating of a product
Create a function called likeProduct. This function will helps
 to like to the product if it is not liked and remove like if it was liked.
 */

function rateProduct(prodBase) {
  return prodBase.sort((a, b) => {
    return averageRating(a.ratings) - averageRating(b.ratings);
  });
}

function averageRating(ratesArr) {
  const rateSum = ratesArr.reduce((acc, { rate }) => {
    return (acc += rate);
  }, 0);
  const rate = rateSum !== 0 ? rateSum / ratesArr.length : 0;
  return rate;
}

function likeProduct(productId, productBase, userId) {
  const product = productBase.find(el => el._id === productId);
  const isLiked = product.likes.includes(userId);
  const likeIndex = product.likes.indexOf(userId);

  if (isLiked) {
    product.likes.splice(likeIndex, 1);
  } else {
    product.likes.push(userId);
  }
}
