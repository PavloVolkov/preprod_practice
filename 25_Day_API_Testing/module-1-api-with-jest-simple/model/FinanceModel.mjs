import AppConfig from '../configs/AppConfig.cjs'

import axios from 'axios'

class FinanceModel {
  // TODO: define parameters to be able to get dynamic test data
  /*
  This method is noneffective because the HTTP request 
  parameters were hardcoded. The Model should support 
  ability to define values for HTTP request parameters. There is a need to develop 
  a method for Finance model to be able to define the following parameters dynamically:
  company
  region
  interval
  range
  */
  static async getFinanceData(
    {
      company = AppConfig.urlSettings.company,
      region = AppConfig.urlSettings.region,
      language = AppConfig.urlSettings.language,
      interval = AppConfig.urlSettings.interval,
      range = AppConfig.urlSettings.range,
    } = AppConfig.urlSettings
  ) {
    const params = `chart/${company}?region=${region}&lang=${language}&includePrePost=false&interval=${interval}&range=${range}&corsDomain=finance.yahoo.com&.tsrc=finance`
    try {
      return await axios.get(AppConfig.baseUrl + params)
    } catch (e) {
      return e.response
    }
  }
}
export default FinanceModel
