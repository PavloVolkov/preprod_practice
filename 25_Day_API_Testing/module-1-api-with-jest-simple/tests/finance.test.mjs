import FinanceModel from '../model/FinanceModel';
import AppConfig from '../configs/AppConfig.cjs';
import Cases from '../configs/Cases.cjs';
describe('As a FinanceModel API user', () => {
  describe('I have to get HTTP response code', () => {
    /**
     Develop 4 test cases and tests for them to verify that the HTTP response code equals 200
    when the following parameters are being put to Finance.getFinanceData() method
    company
    region
    interval
    range
    some combinations of parameters: company + interval, etc...
     */
    Cases.validCases.forEach(el => {
      test(`200 for a valid ${el.validParametr}`, async () => {
        // TODO: replace this method with FinanceModel.getFinanceData(...) method
        // to be able to define different test conditions:
        const { status } = await FinanceModel.getFinanceData(el);
        expect(status).toBe(200);
      });
    });

    test('422 for a invalid range', async () => {
      // TODO: put required parameter to simulate the required test condition
      //develop 1 test case and test for it to verify that the HTTP
      // response code equals 422 when an invalid range is used
      const inValidRange = Cases.inValidCases.invalidRange;
      const { status } = await FinanceModel.getFinanceData({ range: inValidRange });
      expect(status).toBe(422);
    });
    test('404 for a nonexistent company', async () => {
      // TODO: put required parameter to simulate the required test condition
      // develop 1 test case and test for it to verify that the HTTP response code equals
      // 404 when a nonexistent company is uesed
      // TODO: insert verifications
      const inValidCompany = Cases.inValidCases.invalidCompany;
      const { status } = await FinanceModel.getFinanceData({ company: inValidCompany });
      expect(status).toBe(404);
    });
  });

  describe('I have to get values for response headers', () => {
    test('content-type', async () => {
      const { headers } = await FinanceModel.getFinanceData();
      expect(headers['content-type']).toBe('application/json;charset=utf-8');
    });

    test('x-request-id', async () => {
      const { headers } = await FinanceModel.getFinanceData();
      // TODO: insert verifications for "x-request-id" header
      // You have to use regular expression to verify a headers unique value:
      // "d5f838b8-fe84-4f2a-b950-7a07f13264a7"
      let regularExp = /^[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}$/gi;
      // expect(regularExp.test(headers['x-request-id'])).toBe(true)
      expect(headers['x-request-id']).toMatch(regularExp);
    });
  });
});
