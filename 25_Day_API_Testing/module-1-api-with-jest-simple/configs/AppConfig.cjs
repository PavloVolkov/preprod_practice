module.exports = {
  baseUrl: 'https://query1.finance.yahoo.com/v8/finance/',
  urlSettings: {
    company: 'AAPL',
    region: 'US',
    interval: '1m',
    range: '1d',
    language: 'en-US',
  },
  urlSettingsPack: [
    {
      company: 'AAPL',
      region: 'US',
      interval: '1m',
      range: '1d',
      language: 'en-GB',
    },
    {
      company: 'AAPL',
      region: 'AU',
      interval: '15m',
      range: '5d',
      language: 'de',
    },
    {
      company: 'AAPL',
      region: 'CA',
      interval: '5m',
      range: '1mo',
      language: 'en-US',
    },
    {
      company: 'AAPL',
      region: 'FR',
      interval: '1d',
      range: '3mo',
      language: 'fr',
    },
  ],
}
