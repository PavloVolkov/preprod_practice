module.exports = {
  validCases: [
    {
      validParametr: 'company',
      company: 'AAPL',
    },
    {
      validParametr: 'region',
      region: 'AU',
    },
    {
      validParametr: 'interval',
      interval: '5m',
    },
    {
      validParametr: 'range',
      range: '1d',
    },
    {
      validParametr: 'range + interval',
      range: '2d',
      interval: '1h',
    },
  ],
  inValidCases: {
    invalidRange: '100d',
    invalidCompany: 'asd$',
  },
}
