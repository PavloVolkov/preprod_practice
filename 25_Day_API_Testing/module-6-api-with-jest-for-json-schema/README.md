# Module 6

### Description

For this task you have to create API tests to check 
[Yahoo public API](https://query1.finance.yahoo.com/v8/finance/chart/AAPL?region=EQWEQWE&lang=en-US&includePrePost=false&interval=2m&range=1d&corsDomain=finance.yahoo.com&.tsrc=finance). For this task you have to practice JSON Schema validation.
Your test should contain verifications of JSON Schema.

---

### Preparation

1. Clone the template project https://git.epam.com/yevhen_petryk/30-days-of-javascript.git

2. Open `25_Day_API_Testing` directory

  ```sh
  cd 25_Day_API_Testing
  ```

3. Open `module-6-api-with-jest-for-json-schema` directory

  ```sh
  cd module-6-api-with-jest-for-json-schema
  ```

4. Install dependencies

  ```sh
  npm install
  ```

### Project stuff

#### Finance model:

This class contains method to interact with Yahoo API from test scenarios. As example:
- get finance data using GET request

So that, you can define a method once and use it on numerous scenarios.

Use [FinanceModel](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-6-api-with-jest-for-json-schema/model/FinanceModel.mjs) class to define required HTTP requests to Yahoo API.

##### Tests:

This file contains required verifications of API. As example:
- HTTP response body contains a JSON document with specific keys and values
- HTTP response body should have required properties and their values types

Use [finance.test.mjs](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-6-api-with-jest-for-json-schema/tests/finance.test.mjs) file to define required verifications of Yahoo API.

##### Lib:

The [JsonSchemaValidator](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-6-api-with-jest-for-json-schema/lib/JsonSchemaValidator.mjs) 
class uses "jsonschema" library to parse JSON document and validate JSON Schema. Follow the documentation to get familiar with
[jsonschema](https://www.npmjs.com/package/jsonschema) library.

##### Config:

The [AppConfig.cjs](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-3-api-with-jest-for-json/configs/AppConfig.cjs) file contains different configurations of tests. As example, this file contains endpoint of Yahoo API Service. So that, you are able to define it once on one place and change it if necessary.

---

### Specification

Insert verifications to verify that:
- the "chart" property is an object 
- the "currency", "firstTradeDate", "currentTradingPeriod", and "validRanges" properties are present in the response and contain the correct type

Use the [template](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-6-api-with-jest-for-json-schema/tests/finance.test.mjs) for test writing.

---

### Test run

  ```sh
  npm test
  ```

### Expected result

```
As a FinanceModel API user
    I have to get JSON with
        chart object
    I have to get JSON with meta object containing
        all required properties
```

---

### Documentation

Please, use the following manuals to perform tasks:

- https://www.npmjs.com/package/jsonschema
- https://json-schema.org/understanding-json-schema/index.html#
