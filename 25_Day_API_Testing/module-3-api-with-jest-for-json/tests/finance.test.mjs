import FinanceModel from '../model/FinanceModel.mjs';
import JsonBodyParser from '../lib/JsonBodyParser.mjs';
import Cases from '../configs/Cases.cjs';
import { expect } from '@jest/globals';

describe('As a FinanceModel API user', () => {
  describe('I have to get HTTP response body', () => {
    test('with [Symbol] property', async () => {
      const country_PATH = '$.chart.result[0].meta.symbol';
      let { data } = await FinanceModel.getFinanceData();
      //"Symbol" property contains a value corresponding test data: AAPL..
      const value = JsonBodyParser.getValueForPath(data, country_PATH);
      //  TODO: insert verifications to verify that Symbol property contains a value
      //  corresponding test data: AAPL...
      expect(...value).toEqual('AAPL');
    });

    test('with [range] property', async () => {
      // TODO: define valid path to extract data for Range property
      const range_path = '$.chart.result[0].meta.range';
      let { data } = await FinanceModel.getFinanceData();
      const value = JsonBodyParser.getValueForPath(data, range_path);
      const expectedValue = Cases.validRangesArr;
      //  TODO: insert verifications to verify that Symbol property contains a value
      //  corresponding test data: 1d, 2d...
      expect(expectedValue).toEqual(expect.arrayContaining(value));
    });

    test('with [tradingPeriods] property', async () => {
      // TODO: define valid path to extract data for TradingPeriods property
      //"TradingPeriods" property contains a non-empty array
      const tradingPeriods_path = '$.chart.result[0].meta.range';
      let { data } = await FinanceModel.getFinanceData();
      const value = JsonBodyParser.getValueForPath(data, tradingPeriods_path);
      expect(value).not.toEqual([]);
    });

    test('with [CurrentTradingPeriod] property', async () => {
      //"CurrentTradingPeriod" property contains 3 objects: pre, regular, post
      const currentTradingPeriod_path = '$.chart.result[0].meta.currentTradingPeriod';
      let { data } = await FinanceModel.getFinanceData();
      const value = JsonBodyParser.getValueForPath(data, currentTradingPeriod_path);
      const expectedObjectKeys = ['pre', 'regular', 'post'];
      expect(Object.keys(value[0])).toEqual(expectedObjectKeys);
      // expect(...value).toEqual(
      //   expect.objectContaining({
      //     regular: expect.anything(),
      //     post: expect.anything(),
      //     pre: expect.anything(),
      //   }),
      // );
    });
    test('with [error] property', async () => {
      // TODO: define valid path to extract data for error property
      const range_path = '$.chart.error.code';
      const invalidRange = Cases.inValidCases.invalidRange;
      let { data } = await FinanceModel.getFinanceData({ range: invalidRange });
      const value = JsonBodyParser.getValueForPath(data, range_path);
      //  TODO: insert verifications
      // The test should check that code property contains
      // "Unprocessable Entity" value when an invalid range was used.
      expect(...value).toStrictEqual('Unprocessable Entity');
    });
  });
});
