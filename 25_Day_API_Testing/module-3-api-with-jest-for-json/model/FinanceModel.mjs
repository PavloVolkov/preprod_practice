import AppConfig from '../configs/AppConfig.cjs';

import axios from 'axios';

class FinanceModel {
  static async getFinanceData(
    {
      company = AppConfig.urlSettings.company,
      region = AppConfig.urlSettings.region,
      language = AppConfig.urlSettings.language,
      interval = AppConfig.urlSettings.interval,
      range = AppConfig.urlSettings.range,
    } = AppConfig.urlSettings,
  ) {
    const params = `chart/${company}?region=${region}&lang=${language}&includePrePost=false&interval=${interval}&range=${range}&corsDomain=finance.yahoo.com&.tsrc=finance`;
    try {
      return await axios.get(`${AppConfig.baseUrl}${params}`);
    } catch (e) {
      return e.response;
    }
  }
}

export default FinanceModel;
