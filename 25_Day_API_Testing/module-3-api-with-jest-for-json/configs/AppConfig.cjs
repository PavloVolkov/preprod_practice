module.exports = {
  baseUrl: 'https://query1.finance.yahoo.com/v8/finance/',
  urlSettings: {
    company: 'AAPL',
    region: 'en-US',
    interval: '2m',
    range: '1d',
    language: 'en-US',
  },
}
