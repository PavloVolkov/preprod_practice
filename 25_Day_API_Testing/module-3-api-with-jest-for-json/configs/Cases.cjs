module.exports = {
  validRangesArr: ['1d', '5d', '1mo', '3mo', '6mo', '1y', '2y', '5y', '10y', 'ytd', 'max'],
  validCases: [
    {
      validParametr: 'company',
      company: 'AAPL',
    },
    {
      validParametr: 'region',
      region: 'AU',
    },
    {
      validParametr: 'interval',
      interval: '5m',
    },
    {
      validParametr: 'range',
      range: '1d',
    },
    {
      validParametr: 'range + interval',
      range: '2d',
      interval: '1h',
    },
  ],
  inValidCases: {
    invalidRange: '100d',
    invalidCompany: 'asd$',
  },
}
