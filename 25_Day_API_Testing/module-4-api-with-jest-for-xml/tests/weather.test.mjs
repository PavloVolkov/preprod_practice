import WeatherModel from '../model/WeatherModel';
import XmlBodyParser from '../lib/XmlBodyParser';

describe('As a Weather API user', () => {
  describe('I have to get HTTP response body', () => {
    test('with [Title] node', async () => {
      const { data } = await WeatherModel.getWeatherData();
      const title = XmlBodyParser.getTitle(data);
      const expectedTitle = 'New York, NY - AccuWeather.com Forecast';
      // TODO: insert verifications to verify Title node value
      return expect(title).resolves.toBe(expectedTitle);
    });

    test('with [Channel] items', async () => {
      const { data } = await WeatherModel.getWeatherData();
      // TODO: insert verifications to check that every item node contains the following child nodes:
      // - title
      // - link
      // - guid
      // - description
      // - pubDate
      let link = await XmlBodyParser.getItems(data, 'link');
      link = link.nodeName;
      let title = await XmlBodyParser.getItems(data, 'title');
      title = title.nodeName;
      let guid = await XmlBodyParser.getItems(data, 'guid');
      guid = guid.nodeName;
      let description = await XmlBodyParser.getItems(data, 'description');
      description = description.nodeName;
      let pubDate = await XmlBodyParser.getItems(data, 'pubDate');
      pubDate = pubDate.nodeName;
      return expect(link && title && guid && description && pubDate).toBeDefined();
    });
    // TODO: the verifications should check that:
    // - link node value contains a href
    // - guid node has "isPermaLink" attribute      "title",
    test('with [Channel] items = "link", that contains a href', async () => {
      const { data } = await WeatherModel.getWeatherData();
      const link = await XmlBodyParser.getItems(data, 'link');
      const linkHref = link.childNodes[0].data;
      const regExp = new RegExp(/^(?:https)?:\/\/?[\w.-]+(?:\.[\w\.-]).*/gm);
      expect(linkHref).toMatch(regExp);
    });
    test('with [Channel] items = "guid", that has "isPermalink" atribute', async () => {
      const { data } = await WeatherModel.getWeatherData();
      const guid = await XmlBodyParser.getItems(data, 'guid');
      const guidAttribute = guid.attributes[0].name;
      const regExp = new RegExp(/\bisPermaLink\b/gm);
      expect(guidAttribute).toMatch(regExp);
    });
  });
});
