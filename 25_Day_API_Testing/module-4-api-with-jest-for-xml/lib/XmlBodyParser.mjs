import { DOMParser } from 'xmldom';

class XmlBodyParser {
  static async getTitle(httpResponseBody) {
    const documentObject = new DOMParser().parseFromString(httpResponseBody, 'text/xml');
    const channelElement = documentObject.getElementsByTagName('rss')[0].getElementsByTagName('channel')[0];

    const contentPropertiesList = channelElement.childNodes;

    for (let k = 0; k < contentPropertiesList.length; k++) {
      const property = contentPropertiesList.item(k);
      if (property.nodeName === 'title') {
        return property.textContent;
      }
    }
  }

  static async getItems(httpResponseBody, element) {
    // TODO: extract and return Item node list
    /**
      every "/rss/channel/item" node contains the following child nodes:
      title
      link
      guid
      description
      pubDate
      The verifications should check that:
      link node value contains a href. You have to use a regular expression.
      guid node has "isPermaLink" attribute
    */
    const documentObject = new DOMParser().parseFromString(httpResponseBody, 'application/xml');
    const channelElement = documentObject
      .getElementsByTagName('rss')[0]
      .getElementsByTagName('channel')[0]
      .getElementsByTagName('item')[3];

    const contentPropertiesList = channelElement.childNodes;
    for (let k = 0; k < contentPropertiesList.length; k++) {
      const property = contentPropertiesList.item(k);
      if (property.nodeName === element) {
        return property;
      }
    }
  }
}

export default XmlBodyParser;
