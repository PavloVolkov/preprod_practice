# Module 4

### Description

For this task you have to create API tests to check
[AccuWeather.com Live Weather Feeds](https://rss.accuweather.com/rss/liveweather_rss.asp).

---

### Preparation

1. Clone the template project https://git.epam.com/yevhen_petryk/30-days-of-javascript.git

2. Open `25_Day_API_Testing` directory

  ```sh
  cd 25_Day_API_Testing
  ```

3. Open `module-4-api-with-jest-for-xml` directory

  ```sh
  cd module-4-api-with-jest-for-xml
  ```

4. Install dependencies

  ```sh
  npm install
  ```

---

### Project stuff

#### Weather model:

The [WeatherModel](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-4-api-with-jest-for-xml/model/WeatherModel.mjs) 
class contains methods to interact with Weather Feeds from test scenarios:
- get Feed via HTTP request

##### Tests:

These files contain required verifications of API. 
The test [weather.test.mjs](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-4-api-with-jest-for-xml/tests/weather.test.mjs)
for this task contains verifications of XML documents:
- the XML document contains required tags, attributes, etc...

##### Lib:

The [XmlBodyParser](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-4-api-with-jest-for-xml/lib/XmlBodyParser.mjs) 
class uses "xmldom" library to parse XML document and extract data interacting with DOM items.
Follow the documentation to get familiar with 
[xmldom](https://www.npmjs.com/package/xmldom) library.

##### Config:

The [AppConfig.cjs](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-4-api-with-jest-for-xml/configs/AppConfig.cjs) file contains different configurations of tests.
As example, this file contains endpoint of AccuWeather.com Live Weather Feeds.
So that, you are able to define it once on one place and change it if necessary.

--- 

### Specification

Insert verifications to verify that:
1. "Title" node contains expected value
1. every "/rss/channel/item" node contains the following child nodes:
- title
- link
- guid
- description
- pubDate

The verifications should check that:
- link node value contains a href. You have to use a regular expression.
- guid node has "isPermaLink" attribute

Use the [template](https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/25_Day_API_Testing/module-4-api-with-jest-for-xml/tests/weather.test.mjs) for test writing.

---

### Test run

  ```sh
  npm test
  ```

### Expected result

```
As a Weather API user
    I have to get HTTP response body
        with [Title] node
        with [Channel] items 
```

---

### Documentation

Please, use the following manuals to perform tasks:

- https://jestjs.io/docs/getting-started
- https://www.npmjs.com/package/axios
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET
- https://developer.mozilla.org/en-US/docs/Web/XML/XML_introduction
- https://www.npmjs.com/package/xmldom
