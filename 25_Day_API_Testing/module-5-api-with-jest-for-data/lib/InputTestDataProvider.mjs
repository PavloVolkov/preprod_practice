"use strict";

import * as fs from "fs";
import * as path from "path";

const testDataDir = "data/post_data/";

class InputTestDataProvider {
  static getTestData(fileName) {
    let testData = "";

    const filePath = path.join(testDataDir, fileName);

    try {
      testData = fs.readFileSync(filePath, "utf8");
    } catch (e) {
      console.log(e.stack);
    }

    return testData;
  }
}
export default InputTestDataProvider;
