import AppConfig from '../configs/AppConfig.mjs';
import TokenProvider from '../lib/TokenProvider.mjs';
import InputTestDataProvider from '../lib/InputTestDataProvider.mjs';
import axios from 'axios';

class PasteModel {
  static async postPastePlainData(dataFileName, tokenName) {
    const token = TokenProvider.getToken(tokenName);
    const data = InputTestDataProvider.getTestData(dataFileName);
    const api_option = 'paste';
    try {
      return await axios.post(
        AppConfig.baseUrl,
        `api_dev_key=${token}&api_paste_code=${data}&api_option=${api_option}`,
      );
    } catch (e) {
      return e.response;
    }
  }

  static async postPaste(tokenName, data) {
    const api_option = 'paste';
    const expire_date = '10M';
    try {
      return await axios.post(
        AppConfig.baseUrl,
        `api_dev_key=${tokenName}&api_paste_code=${data}&api_option=${api_option}&api_paste_expire_date=${expire_date}`,
      );
    } catch (e) {
      return e.response;
    }
  }
}
export default PasteModel;
