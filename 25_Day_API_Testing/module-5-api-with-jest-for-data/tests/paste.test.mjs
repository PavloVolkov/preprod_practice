import PasteModel from '../model/PasteModel.mjs';
import InputTestDataProvider from '../lib/InputTestDataProvider.mjs';
import TokenProvider from '../lib/TokenProvider.mjs';
import { expect } from '@jest/globals';
describe('As a Paste API user', () => {
  describe('I need to be able to create Paste and get', () => {
    test('200 for a valid data', async () => {
      const { status } = await PasteModel.postPastePlainData('case1.json', 'pastbin_token_1');
      // TODO: received data contains URL of created Paste: "https://pastebin.com/C3ZZxfm7"
      // put your verifications here
      // ...
      expect(status).toBe(200);
    });
  });

  describe('I need to be able to create Paste with', () => {
    test('10 min expiration time', async () => {
      // TODO: Develop "getTestData" method to extract file content from files located on "<projectDir>/data/post_data" directory
      // to be able to define POST body dynamically
      const token1 = TokenProvider.getToken('pastbin_token_1');
      const testData = InputTestDataProvider.getTestData('case1.json');
      const data = await PasteModel.postPaste(token1, testData);
      const pattern = /(&api_paste_expire_date=10M)/gm;
      // Put your verifications here. You need to verify that the created Paste has been created with 10 min
      // expiration timeout
      // ...
      expect(data).toMatch(pattern);
    });
  });
});
