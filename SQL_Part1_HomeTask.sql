
--Part 1
--1) Update the Employees table, so it cONtains the HireDate values FROM 2017 till the current (2021) year.
  UPDATE employees 
  SET HireDate=date_add(DATE(HireDate), interval 1 year)
--2) Delete records FROM the Products table which weren't ORDERed. 
  ALTER TABLE ORDER_details
  DROP FOREIGN KEY FK_ORDER_Details_Products;
  DELETE FROM products WHERE UnitsONORDER = 0;

--3) Get the list of data about employees: First Name, Last Name, Title, Hire Date who was hired this year (2019).
  SELECT FirstName, LastName, HireDate, Title  FROM employees
  WHERE YEAR(HireDate) = YEAR(CURRENT_DATE());

--4) Find quantity of employees in each department. Note: Departments is the same as a title in the Employees table 
  SELECT Title, count(EmployeeID) AS employees_quantity FROM employees
  GROUP BY Title;

--5) Get the list of suppliers, which are located in USA and have a specified regiON.
  SELECT SupplierID, CompanyName, Country, RegiON FROM suppliers
  WHERE RegiON IS NOT NULL AND Country = 'USA'

--6) Get the amount of products that were delivered BY each supplier (company), which have a discount FROM the Unit Price more than 10%. 
--WHERE record are represented FROM the biggest to lowest discount.
  SELECT SupplierID, count(SupplierID), UnitPrice FROM products
  WHERE UnitPrice > 10
  GROUP BY SupplierID
  ORDER BY UnitPrice DESC

--7) Get the top five product categories with the list of the
-- most buyable products in Germany.
SELECT CategoryName, ProductName
FROM products
INNER JOIN suppliers
ON products.SupplierID = suppliers.SupplierID 
INNER JOIN categories
ON products.CategoryID = categories.CategoryID 
WHERE country ='Germany'
ORDER BY UnitsInStock DESC LIMIT 5

--8) Get the First Name, Last Name and Title of Managers and their subordinates.
SELECT firstName, lastName, title FROM employees

--9) Get the Firts Name, Last Name, Title of Sales who has the least amount of ORDERs. (Amount of sold products should be also in the result set).
SELECT  FirstName, LastName, Title,  COUNT(ORDERs.ORDERID) AS NumberOfORDERs
FROM employees 
INNER JOIN ORDERs
ON employees.EmployeeID = ORDERs.EmployeeID
GROUP BY LastName
ORDER BY NumberOfORDERs LIMIT 1