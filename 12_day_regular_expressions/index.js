// 1 Calculate the total annual income of
// the person from the following text.
//‘He earns 4000 euro from salary per month,
//10000 euro annual bonus, 5500 euro online courses per month.’
const str =
  "He earns 4000 euro from salary per month, 10000 euro annual bonus,5500 euro online courses per month.";
const income = str.match(/\d+/g).reduce((acc, el) => acc + parseInt(el), 0);
// console.log(income)

//2 Write a pattern which identify if a string is a
//valid JavaScript variable

function isValidVariable(str) {
  let rexexp = /^(?!\d)([a-zA-Z\_\d]+)$/g;
  console.log(rexexp.test(str));
}
// isValidVariable('first_name'); // true
// isValidVariable('first-name'); // false
// isValidVariable('1first_name'); // false
// isValidVariable('firstname'); // true

// 3 Write a function called tenMostFrequentWords
//which get the ten most frequent word from a string:
const paragraph = `I love teaching. If you do not love teaching what else can you love. I love Python if you do not love something which can give you all the capabilities to develop an application what else can you love.`;
function tenMostFrequentWords(string, arrSize) {
  let frequent = getOnlyWords(string);
  frequent = frequent.reduce((acc, el) => {
    if (!wordInAcc(acc, el)) {
      acc.push({ word: el, count: howMuchIsRepeted(string, el) });
    }
    return acc;
  }, []);
  return arrCutTo(sortingByCount(frequent), arrSize);
}
// console.log(tenMostFrequentWords(paragraph, 10, getOnlyWords));

// console.log(tenMostFrequentWords(paragraph));
// // console.log(tenMostFrequentWords(paragraph, 10));
// 4 Write a function which cleans text. After cleaning,
// count three most frequent words in the string.
const sentence = `%I $am@% a %tea@cher%, &and& I lo%#ve %tea@ching%;. There $is nothing; &as& mo@re rewarding as educa@ting &and& @emp%o@wering peo@ple. ;I found tea@ching m%o@re interesting tha@n any other %jo@bs. %Do@es thi%s mo@tivate yo@u to be a tea@cher!?`;
function cleanText(string) {
  const cleansStr = stringCleaner(string);
  const newStr = getOnlyWords(cleansStr);
  const frequent = newStr.reduce((acc, el) => {
    if (!wordInAcc(acc, el)) {
      acc.push({ word: el, count: howMuchIsRepeted(cleansStr, el) });
    }
    return acc;
  }, []);
  return arrCutTo(sortingByCount(frequent), 3);
}
// console.log(cleanText(sentence));

function stringCleaner(str) {
  return str.replace(/[\%,\$,\&,\@,\;,\#]/gi, "");
}
function wordInAcc(arr, wordInArr) {
  return arr.find(({ word }) => wordInArr === word);
}
function howMuchIsRepeted(str, word) {
  const regExp = new RegExp(`\\b${word}\\b`, "g");
  return str.match(regExp).length;
}
function getOnlyWords(str) {
  return str.match(/\w+/g);
}
function sortingByCount(arr) {
  return arr.sort((a, b) => b.count - a.count);
}
function arrCutTo(arr, num) {
  const cutedArr = [];
  arr.every((el) => {
    cutedArr.push(el);
    if (cutedArr.length >= num) return false;
    else return true;
  });
  return cutedArr;
}
