Additional task (*) 
Write common sides and differences between merge and rebase commands. 
Merge creates new merge commit that contains all commit from bouth branch
Rebase makes the same but without new commit, and if we have 2 branch x and y and exec command git rebase y (when current x branch)
all commits from x moved to y branch. 


Write pros and cons for merge and rebase. Describe situations for preferably usage of these commands. 
Merge: 
+ it is simple to understand what is going on
  saves all history of commits

- saves all history of commits

Rebase:
+ simplify work with history of commits.

- if rebase makes in the wrong way the history of commits changes it lead to a some sortof problems.