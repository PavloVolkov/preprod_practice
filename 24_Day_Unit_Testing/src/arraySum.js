/**
//* It recieves an array of strings, integers and array like itself.
//* Return the summary of all integers in it on * any level.
 *
 * @param {Array} elements
 * @returns {number} summary of all integers or 0 in other cases
 */
function arraySum(elements) {
  // PLACE YOUR CODE BETWEEN THIS...
  return getSumOfDigits(elements);
  // ...AND THIS COMMENT LINE!
}

function getSumOfDigits(params) {
  let sum = 0;
  function arrUnpackRecursion(paramsArr) {
    paramsArr.forEach(el => {
      if (Array.isArray(el)) {
        arrUnpackRecursion(el);
      }
      if (!Array.isArray(el) && typeof el === 'number') {
        sum += el;
      }
    });
  }
  arrUnpackRecursion(Array.from(arguments));
  return sum;
}

module.exports = arraySum;
