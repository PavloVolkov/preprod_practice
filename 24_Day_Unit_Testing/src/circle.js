/**
 * Create the Circle class.
 *
 * @typedef {object} Circle
 * @property {number} radius the radius of the circle
 * @function getArea return the area of the circle
 * @function getPerimeter return the perimeter of the circle
 */

class Circle {
  constructor(radius) {
    if (radius > 0) {
      this.r = radius;
    } else {
      throw new Error('[Error] Radius must be greater than 0');
    }
  }
  getArea() {
    return this.r * this.r * Math.PI;
  }
  getPerimeter() {
    return 2 * this.r * Math.PI;
  }
}
module.exports = Circle;
