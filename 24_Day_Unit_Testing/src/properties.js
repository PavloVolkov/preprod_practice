/**
 * It returns the property names of the given object.
 *
 * @param {object} obj the object
 * @returns {string[]} the list of the properties
 * // of the object or empty array if it
 * // is not an object
 */

function getProperties(obj) {
  return typeof obj === 'object' ? Object.getOwnPropertyNames(obj) : [];
}
module.exports = getProperties;
