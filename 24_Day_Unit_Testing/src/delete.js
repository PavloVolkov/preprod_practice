/**
 * It deletes the given property of the given object
 * and returns the new object.
 * The function must not modify the original object!
 *
 * @param {object} obj the object
 * @param {string} key the name of the property to delete
 * @returns {object} the new object without the given property
 */

// PLACE YOUR CODE BETWEEN THIS...
function del(obj, key) {
  if (obj && key) {
    const nObj = { ...obj };
    delete nObj[`${key}`];
    return nObj;
  } else {
    throw new Error('[Error] Please, provide an object as a parameter');
  }
}
// ...AND THIS COMMENT LINE!
module.exports = del;
