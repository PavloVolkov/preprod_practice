/**
 * It returns the camel-case version of string.
 * E.g.: simple lowercase string => simpleLowercaseString
 *
 * @param {string} toConvert
 * @returns {string} camel-case string or empty string in other cases
 */

function toCamelCase(toConvert) {
  // PLACE YOUR CODE BETWEEN THIS...
  if (typeof toConvert !== 'string') {
    return '';
  }
  toConvert = Array.from(arguments)
    .toString()
    .replace(/[\n,\t,\!,\?]/gi, '')
    .match(/\w+/gi, ' ')
    .map((e, index) => {
      let cap, firstletter;
      if (e !== '') {
        cap = e.toLowerCase().split('');
        if (index === 0) {
          firstletter = cap[0].toLowerCase();
        }
        if (index !== 0) {
          firstletter = cap[0].toUpperCase();
        }
        cap.splice(0, 1, firstletter);
        return cap.join('');
      }
    })
    .join('');
  return toConvert;
  // ...AND THIS COMMENT LINE!
}
module.exports = toCamelCase;
