/**
 * It reverses the given object, i.e. return a copy of the object
 * where the keys have become the values and the values the keys
 *
 * @param {object} obj the object
 * @returns {object} the new object
 */
function reversObj(obj) {
  if (isObject(obj)) {
    let newObj = Object.assign({}, obj);
    let entries = Object.entries(newObj);
    let cloneObj = {};
    entries.forEach(el => {
      cloneObj[`${el[1].toString()}`] = el[0];
    });
    return cloneObj;
  }
  if (!isObject(obj)) {
    throw new Error('[Error] Please, provide an object as a parameter');
  }
}

function isObject(val) {
  return typeof val === 'function' || typeof val === 'object';
}

module.exports = reversObj;
