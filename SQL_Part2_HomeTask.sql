--1) ClONe data FROM the Shippers table to the NewShippers table.
  CREATE TABLE NewShippers SELECT * FROM Shippers;

--2) Get the list of suppliers which are related to each product name which has price greater 
--than or equal 15$.
--( Which informatiON about supplier will be present in result set is optiONal)
  SELECT UnitPrice, ProductName, CompanyName, suppliers.SupplierID
  FROM products
  INNER JOIN suppliers
  ON products.SupplierID = suppliers.SupplierID
  WHERE UnitPrice >= 15

--3) Get the list of total quantities of ordered products which cONsists of: 
--total quantity ordered in Germany AND the total quantity of products ordered in Sweden. (Result should cONtain 2 rows)
SELECT country, sum(UnitsONOrder)
FROM products
INNER JOIN suppliers
ON products.SupplierID = suppliers.SupplierID
WHERE country ='Italy' or country = 'sweden'
GROUP BY country

--4) Find the list of different countries in Employees AND Customers tables.  
-- 4.1
SELECT customers.Country
FROM customers
LEFT JOIN employees
ON customers.country != employees.country
WHERE employees.country IS NOT NULL
GROUP BY Country

--5) Find the list of the same Postal Codes between Suppliers AND Customers tables.\
--5.2
SELECT suppliers.PostalCode 
FROM suppliers
INNER JOIN customers
ON suppliers.PostalCode = customers.PostalCode

--6) Find the top RegiON, City AND Country FROM which sales specialists 
--were hired (means who sold the biggest quantity of products).
SELECT  RegiON, COUNT(Orders.OrderID) AS NumberOfOrders
FROM employees 
INNER JOIN orders
ON employees.EmployeeID = orders.EmployeeID
GROUP BY RegiON 
ORDER BY NumberOfOrders DESC

SELECT  City, COUNT(Orders.OrderID) AS NumberOfOrders
FROM employees 
INNER JOIN orders
ON employees.EmployeeID = orders.EmployeeID
GROUP BY City 
ORDER BY NumberOfOrders DESC

SELECT  Country, COUNT(Orders.OrderID) AS NumberOfOrders
FROM employees 
INNER JOIN orders
ON employees.EmployeeID = orders.EmployeeID
GROUP BY Country 
ORDER BY NumberOfOrders DESC

--7) Get two lists of products: with a price < 50.00 with a discountinued flag AND < 50  without a discountinued flag.
SELECT * FROM products
WHERE UnitPrice <= 50 AND DiscONtinued = 1

SELECT * FROM products
WHERE UnitPrice <= 50 AND DiscONtinued = 0

--8) Create new table NewProducts based ON the Products table with ONly discountinued products. 
CREATE TABLE newProducts SELECT * FROM products WHERE DiscONtinued = 1;

-- Compare data sets between Products AND NewProducts tables. 
SELECT * FROM newProducts
UNION ALL
SELECT * FROM Products

--(Check that ONly discountinued products are inserted).
SELECT DiscONtinued FROM newProducts 

--The 9th query is optiONal 

--Get the list of orders, WHERE a required date is bigger than the Shipped date ( compare in days) AND Ship RegiON is not specified.
SELECT * FROM orders
WHERE ShipRegiON IS NULL AND RequiredDate > ShippedDate