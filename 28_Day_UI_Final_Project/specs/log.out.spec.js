import { expect } from 'chai';
import AppConfig from '../configs/AppConfig';
import { user } from '../models/user';
import { HomePage } from '../page_objects/home.page';
import { SignInPage } from '../page_objects/sign.in.page';
import { SettingsPage } from '../page_objects/settings.page';
import { ApiHelper } from '../utils/api.helper';

describe('User is able to log out', () => {
  let homePage;
  let signInPage;
  let settings;

  before(async () => {
    const token = await ApiHelper.createUser(user); // Create a new user via API
    await ApiHelper.loginToApp(token); // Login to application (use method ApiHelper.loginToApp)

    homePage = new HomePage();
    signInPage = new SignInPage();
    settings = new SettingsPage();

    await homePage.open();
    await homePage.waitHomePageLoaded();
  });

  it('Verify that the Home page is open', async () => {
    await homePage.clickSettingsButton(); //  Open the Settings page
    await settings.clickLogOutButton(); // Click on the Log Out button
    const pageUrl = await homePage.getPageUrl();

    expect(pageUrl).to.eq(`${AppConfig.webUrl}/`); //Verify that the Home page is open
    const pageTitle = await homePage.getPageTitle(); // Verify that the Home page is open
    expect(pageTitle).to.eq('Conduit');
    const pageHeader = await signInPage.getPageHeader(); // Verify that the Home page is open
    expect(pageHeader).to.eq('conduit');
  });
  it("Verify that the Header contains the following links - 'Home', 'Sign in', 'Sign up'", async () => {
    const navLinksTextArray = await homePage.navBar.getNavLinksText();
    const expectedArray = ['Home', 'Sign in', 'Sign up'];
    expect(navLinksTextArray).to.have.members(expectedArray); //Verify that the Header contains the following links - 'Home', 'Sign in', 'Sign up' */
  });
});

/**
 * ##### User is able to log out

1. Create a new user via API
1. Login to application (use method ApiHelper.loginToApp)
1. Open the Settings page
1. Click on the Log Out button
1. Verify that the Home page is open
1. Verify that the Header contains the following links - 'Home', 'Sign in', 'Sign up'
 */
