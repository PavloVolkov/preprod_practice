import { expect } from 'chai';
import { user } from '../models/user';
import { ProfilePage } from '../page_objects/profile.page';
import { ApiHelper } from '../utils/api.helper';
import { globalConfig } from '../api/global';

describe('User is able to view Favorited articles on the Profile page', () => {
  let profile;

  before(async () => {
    const token = await ApiHelper.createUser(user); // Create a new user via API
    await ApiHelper.postNewArticle(token); // Create a new article via API
    const firstArticle = await ApiHelper.addFirstArticleToFavorite(token); //Add created article to favorite via API
    await ApiHelper.loginToApp(token); // Login to application (use method ApiHelper.loginToApp)

    profile = new ProfilePage();

    const userProfileName = firstArticle.author.username;
    await profile.open(userProfileName);
    await profile.waitProfilePageLoaded();
  });

  it('there is one article', async () => {
    await profile.clickFavoriteArticlesTab();

    const articleList = await profile.articlesList;
    expect(articleList).to.have.lengthOf(1);
  });
  it('the article contains correct title', async () => {
    const titles = await profile.getArticleTitlesText();
    const requiredTitle = globalConfig.requestBody.article.title;
    expect(titles[0]).to.eq(requiredTitle);
  });
});

/**##### User is able to view Favorited articles on the Profile page
1. Create a new user via API
1. Create a new article via API
1. Add created article to favorite via API
1. Login to application (use method ApiHelper.loginToApp)
1. Open the profile page
1. Click on the Favorited Articles Tab
1. Verify that there is one article
1. Verify that the article contains correct title
*/
