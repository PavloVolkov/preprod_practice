import { expect } from 'chai';
import { user } from '../models/user';
import { ArticlePage } from '../page_objects/article.page';
import { ApiHelper } from '../utils/api.helper';

describe('User is able to delete article comment on the Article Details page', () => {
  let article;

  before(async () => {
    const token = await ApiHelper.createUser(user); // Create a new user via API
    await ApiHelper.postNewArticle(token); // Create a new article via API
    const articleId = await ApiHelper.addCommentTofirstArticle(token); // Add comment to article via API
    await ApiHelper.loginToApp(token); // Login to application (use method ApiHelper.loginToApp)

    article = new ArticlePage();
    await article.open(articleId); //Open the Article Details page for the previously created article
    await article.waitArticlePageLoaded();
  });

  it('delete article comment', async () => {
    await article.clickDeleteCommentButton(); // Click on the delete comment icon
    const waitForDisplayedReverse = true;
    const timeout = 3000;
    await article.waitForDisplayed(article.cardBlock, timeout, waitForDisplayedReverse); // wait for the comments block is removed
    const commentExists = await article.isDisplayedCardBlock();
    expect(commentExists).to.be.false; //Verify that there are no commets for the opened artcile*/
  });
});

/** ##### User is able to delete article comment on the Article Details page

1. Create a new user via API
1. Create a new article via API
1. Add comment to article via API
1. Login to application (use method ApiHelper.loginToApp)
1. Open the Article Details page for the previously created article
1. Click on the delete comment icon
1. Verify that there are no commets for the opened artcile*/
