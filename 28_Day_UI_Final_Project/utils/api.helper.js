import { User } from '../api/service/user';
import { HomePage } from '../page_objects/home.page';
import { Axios } from '../utils/axios';
import { globalConfig } from '../api/global';

export class ApiHelper {
  static async createUser(user) {
    const { data } = await browser.call(async function () {
      return await User.register(user);
    });

    const jwtToken = data?.user?.token;

    if (!jwtToken) {
      throw new Error('Did not receive user JWT Token in response');
    }

    return jwtToken;
  }

  static async signInUser(user) {
    const { data } = await browser.call(async function () {
      return await User.login(user);
    });

    const jwtToken = data?.user?.token;

    if (!jwtToken) {
      throw new Error('Did not receive user JWT Token in response');
    }

    return jwtToken;
  }

  static async loginToApp(token) {
    const homePage = new HomePage();
    await homePage.open();

    await browser.execute(
      function (key, value) {
        window.localStorage.setItem(key, value);
      },
      'id_token',
      token,
    );

    await homePage.refreshCurrentPage();
    await homePage.waitHomePageLoaded();
  }
  static async postNewArticle(token) {
    const endpointArticles = globalConfig.endpointArticles;
    const header = globalConfig.header(token);
    const requestBody = globalConfig.requestBody;

    await Axios.post(endpointArticles, header, requestBody);
  }
  static async addCommentTofirstArticle(token) {
    const endpointArticles = globalConfig.endpointArticles;
    const header = globalConfig.header(token);

    const { data } = await Axios.get(endpointArticles, header);
    const firstArticles = data.articles[0];
    const articleId = firstArticles.slug;

    const commentBody = globalConfig.commentBody;

    const endpointComment = globalConfig.endpointToComment(articleId);

    await Axios.post(endpointComment, header, commentBody); // Add comment to article via API
    return articleId;
  }
  static async addFirstArticleToFavorite(token) {
    const header = globalConfig.header(token);
    const endpointArticles = globalConfig.endpointArticles;

    const { data } = await Axios.get(endpointArticles, header);

    const firstArticle = data.articles[0];
    firstArticle.favorited = true;
    const editedRequestBody = { article: firstArticle };
    const articleId = firstArticle.slug;
    const endpointFavorite = globalConfig.endpointToFavorite(articleId);

    await Axios.post(endpointFavorite, header, editedRequestBody); //Add created article to favorite via API
    return firstArticle;
  }
}
