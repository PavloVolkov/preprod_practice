import { BasePage } from './base.page';

export class SettingsPage extends BasePage {
  get root() {
    return $('div.settings-page');
  }

  get logOutButton() {
    return $('//button[contains(text(), "Or click here to logout.")]');
  }

  async open() {
    await super.open('/');
  }

  async clickLogOutButton() {
    await this.logOutButton.click();
  }

  async waitSettingsPageLoaded() {
    await this.waitForDisplayed(this.root);
  }
}
