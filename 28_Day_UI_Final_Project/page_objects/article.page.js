import { BasePage } from './base.page';

export class ArticlePage extends BasePage {
  get root() {
    return $('div[data-qa-id="article-page"]');
  }

  get deleteCommentButton() {
    return $('span.mod-options i.ion-trash-a');
  }

  get cardBlock() {
    return $('div.card');
  }

  isDisplayedCardBlock() {
    return this.cardBlock.isDisplayed();
  }

  async clickDeleteCommentButton() {
    await this.deleteCommentButton.click();
  }

  async open(articleId) {
    await super.open(`/articles/${articleId}/`);
  }

  async waitArticlePageLoaded() {
    await this.waitForDisplayed(this.root);
  }
}
