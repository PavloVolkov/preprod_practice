import { BasePage } from './base.page';

export class ProfilePage extends BasePage {
  get root() {
    return $('div.profile-page');
  }

  get articlesList() {
    return $$('div[class="article-preview"] a.preview-link');
  }

  get articleTitles() {
    return $$('div[class="article-preview"] a.preview-link h1');
  }

  get favoriteArticlesTab() {
    return $('//a[contains(text(), "Favorited Articles")]');
  }

  async clickFavoriteArticlesTab() {
    await this.favoriteArticlesTab.click();
  }

  async open(profileName) {
    await super.open(`/@${profileName}/`);
  }

  async getArticleTitlesText() {
    const elNumber = (await this.articleTitles).length;
    const textArr = [];
    for (let i = 0; i < elNumber; i++) {
      textArr.push((await this.articleTitles[i].getText()).trim());
    }
    return textArr;
  }

  async waitProfilePageLoaded() {
    await this.waitForDisplayed(this.root);
  }
}
