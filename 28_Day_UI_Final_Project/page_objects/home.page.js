import { BasePage } from './base.page';

export class HomePage extends BasePage {
  get root() {
    return $('div.home-page');
  }
  get settingsButton() {
    return $('//a[contains(@href, "/settings")]');
  }

  get userProfile() {
    return $('//ul[@data-qa-id="site-nav"]//a[contains(@href, "/@")]');
  }

  async clickUserProfile() {
    await this.userProfile.click();
  }

  async clickSettingsButton() {
    await this.settingsButton.click();
  }

  async open() {
    await super.open('/');
  }

  async waitHomePageLoaded() {
    await this.waitForDisplayed(this.root);
  }
}
