import AppConfig from '../configs/AppConfig';

export const globalConfig = {
  baseUrl: AppConfig.apiUrl,
  commonHeader: {
    Accept: 'application/json, text/plain, */*',
    'Content-type': 'application/json;charset=UTF-8',
  },
  authorizationHeader(token) {
    return {
      Authorization: `Token ${token}`,
    };
  },
  header(token) {
    return {
      Accept: 'application/json, text/plain, */*',
      'Content-type': 'application/json;charset=UTF-8',
      Authorization: `Token ${token}`,
    };
  },
  endpointArticles: '/articles',
  endpointComment: '/comments',
  endpointFavorite: '/favorite',

  endpointToComment(articleId) {
    return `/articles/${articleId}/comments`;
  },
  endpointToFavorite(articleId) {
    return `/articles/${articleId}/favorite`;
  },
  commentBody: { comment: { body: 'comment via API' } },
  requestBody: {
    article: {
      author: {},
      title: 'Title NOT edited',
      description: 'description NOT edited',
      body: 'Body NOT edited',
      tagList: [],
    },
  },
};
