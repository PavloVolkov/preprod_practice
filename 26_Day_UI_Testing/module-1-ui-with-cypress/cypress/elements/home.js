export const home = {
  futureCarouselBlock: 'div#YDC-Lead',
  // TODO: put getting the element using CSS selector for block marked with 2 on screenshot 1
  // Screenshot - https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/26_Day_UI_Testing/module-1-ui-with-cypress/finance.jpg
  videoListBlock: 'div#Col1-0-ThreeAmigos-Proxy div[data-test="three-amigos-lucky"] > div > ul',
  // TODO: put getting the element using CSS selector for block marked with 3 on screenshot 1
  // Screenshot - https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/26_Day_UI_Testing/module-1-ui-with-cypress/finance.jpg
  newsArticleListBlock: 'div#slingstoneStream-0-Stream',
  // TODO: put getting the element using CSS selector for block marked with 4 on screenshot 1
  // Screenshot - https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/26_Day_UI_Testing/module-1-ui-with-cypress/finance.jpg
  cryptocurrenciesListBlock: '#data-util-col [data-yaft-module="tdv2-applet-crypto_currencies"]',
  // TODO: put getting the element using CSS selector for elements marked with 5 on screenshot 1
  // Screenshot - https://git.epam.com/yevhen_petryk/30-days-of-javascript/-/blob/master/26_Day_UI_Testing/module-1-ui-with-cypress/finance.jpg
  menuItemElements: 'div#YDC-Nav',
  myPortfolio: '#Nav-0-DesktopNav [data-subnav-type="networknav_root-2"]',
  watchlist: '#Nav-0-DesktopNav [data-subnav-type="networknav_root-1"]',
  search: {
    // TODO: put getting the element using CSS selector for search input
    input: '#yfin-usr-qry',
    // TODO: put getting the element using CSS selector for search button
    button: '#header-desktop-search-button',
  },
};
