import FinanceIndexPage from '../page_objects/FinanceIndexPage';
import Rates from '../page_objects/Rates';
/**
 * #### Pagination, work with select, work with hidden elements
1. Open https://finance.yahoo.com/rates using main menu
1. Click at Investing tab
1. Define All option for Investment type select
1. Follow to the 2nd page using the paginator
1. Find the last item on the list
1. Rolldown the details block clicking on the View Details link
1. Verify that Details block contains at least one feature
 */
describe('As a Yahoo Finance site user', function () {
  describe('Pagination, work with select, work with hidden elements', function () {
    before(async function () {
      await FinanceIndexPage.open('/');
    });
    it('Open https://finance.yahoo.com/rates using main menu', async function () {
      await FinanceIndexPage.openRates();
      const frame = await FinanceIndexPage.iFrameTo30YearFixed;
      await browser.switchToFrame(frame);
    });
    it('Click at Investing tab', async function () {
      await Rates.clickInvestingTab();
    });
    it('Define All option for Investment type select', async function () {
      await Rates.clickInvestmentsTypeList();
      const selectList = await Rates.investmentsTypeListElements;
    });
    it('Follow to the 2nd page using the paginator', async function () {
      await Rates.clickInvestingTab();
    });
    it('Find the last item on the list', async function () {
      await Rates.clickPaginationButtonNextPage();
    });
    it('Verify that Details block contains at least one feature', async function () {
      await Rates.expandDitails();
      const ditailsList = await Rates.ditailsList; //Rolldown the details block clicking on the View Details link
      await expect(ditailsList).toBeElementsArrayOfSize({ gte: 1 });
    });
    after(async function () {
      await FinanceIndexPage.close();
    });
  });
});
