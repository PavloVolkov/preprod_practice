import FinanceIndexPage from '../page_objects/FinanceIndexPage';
import Rates from '../page_objects/Rates';
import Mortgage from '../page_objects/Mortgage';
/** #### Form inputs manipulations, valid zip code
1. Open https://finance.yahoo.com/rates using main menu
1. Click at Mortgage tab
1. Click on Purchase button
1. Type value "10001" for Zip Code input
1. Verify that Zip Code input contains "New York, NY" value
1. Verify that Zip Code input is marked as a field containing valid value
 */
describe('As a Yahoo Finance site user', function () {
  describe('Form inputs manipulations, valid zip code', function () {
    before(async function () {
      await FinanceIndexPage.open('/');
    });

    it('Click at Mortgage tab', async function () {
      await FinanceIndexPage.openRates();
      const frame = await FinanceIndexPage.iFrameTo30YearFixed;
      await browser.switchToFrame(frame);
      await Rates.clickMoreBtn();
      await Mortgage.clickMortgageTab();
    });
    it('Click on Purchase button', async function () {
      await Mortgage.clickPurchaseBtn();
    });

    it('Type value "10001" for Zip Code input', async function () {
      await Mortgage.setValueZipField('10000');
    });

    it('is zip code is valid', async function () {
      const text = await Mortgage.textMarkerZipField;
      await expect(text).toHaveText('Invalid zipcode');
    });

    it('Verify that Zip Code input is marked as a field containing invalid value', async function () {
      const myInput = Mortgage.zipInputWrapper;
      await expect(myInput).toHaveAttributeContaining('class', 'error--text');
    });

    after(async function () {
      await FinanceIndexPage.close();
    });
  });
});
