import FinanceIndexPage from '../page_objects/FinanceIndexPage';
import Rates from '../page_objects/Rates';
import PersonalLoans from '../page_objects/PeresonalLoans';
/**#### Drag n drop
1. Open https://finance.yahoo.com/rates using main menu
1. Click at Personal Loans tab
1. Define "1000" value for Personal Loans input using the slider
1. Verify that the result table contains at least one item
1. Verify that all items of the result table contains Max cell with a value that does not exceed value "100k" */
describe('As a Yahoo Finance site user', function () {
  before(async function () {
    await FinanceIndexPage.open('/');
  });
  describe('Drag n drop', function () {
    it('open Rates', async function () {
      await FinanceIndexPage.openRates();
    });
    it('click on personal loans', async function () {
      const frame = await FinanceIndexPage.iFrameTo30YearFixed;
      await browser.switchToFrame(frame);
      await Rates.clickMoreBtn();
      await PersonalLoans.clickPersonalLoansTab();
    });
    it('drag slider', async function () {
      await PersonalLoans.sliderDrag({ x: -80, y: 0 });
    });
    it('Verify that all items of the result table contains Max cell with a value that does not exceed value "100k"', async function () {
      const list = await PersonalLoans.loansResultList;
      let morethan100 = false;
      for (let i = 0; (await list[i]) !== undefined; i++) {
        const listElText = await list[i].getText();
        let digitsValue;
        if (listElText.split('').length > 4) {
          digitsValue = listElText.slice(1, 4);
        } else {
          digitsValue = listElText.slice(1, 3);
        }
        if (digitsValue > 100) {
          res = true;
        }
      }
      await expect(morethan100).toBe(false);
    });
    it('Does the list has at least one item', async function () {
      const list = await PersonalLoans.loansResultList;
      const listLength = await list.length;
      await expect(listLength).toBeGreaterThan(1);
    });

    after(async function () {
      await FinanceIndexPage.close();
    });
  });
});
