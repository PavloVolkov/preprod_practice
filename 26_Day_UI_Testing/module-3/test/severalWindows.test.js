import FinanceIndexPage from '../page_objects/FinanceIndexPage';
import Rates from '../page_objects/Rates';
import Mortgage from '../page_objects/Mortgage';
/**#### Work with several windows
1. Open https://finance.yahoo.com/rates using main menu
1. Click at Mortgage tab
1. Click on Purchase button
1. Type value "10001" for Zip Code input
1. Type value "1000000" for Property Value input
1. Type value "25" for Property Value input
1. Find Learn More button leading to welcome.better.com site and click at this one
1. Verify that the opened window has https://welcome.better.com/ url
1. Close the new opened window
1. Click at Investing tab */
describe('As a Yahoo Finance site user', function () {
  describe('Work with several windows', function () {
    before(async function () {
      await FinanceIndexPage.open('/');
    });

    it('Click at Mortgage tab', async function () {
      await FinanceIndexPage.openRates();
      const frame = await FinanceIndexPage.iFrameTo30YearFixed;
      await browser.switchToFrame(frame);
      await Rates.clickMoreBtn();
      await Mortgage.clickMortgageTab();
    });

    it('Click on Purchase button', async function () {
      await Mortgage.clickPurchaseBtn();
    });

    it('Type value "10001" for Zip Code input', async function () {
      await Mortgage.setValueZipField('10001');
      const elem1 = await Mortgage.zipField;
      await elem1.clearValue();
    });

    it('Type value "1000000" for Property Value input', async function () {
      await Mortgage.clickPropertyValueField();
      await Mortgage.clearElementValue('#input-111');
      await Mortgage.setValuePropertyValueField('1000000');
    });

    it('Type value "25" for Property Value input', async function () {
      await Mortgage.clearElementValue('#input-111');
      await Mortgage.setValuePropertyValueField('25');
    });

    it('Find Learn More button leading to www.tdameritrade.com site and click at this one', async function () {
      await Rates.clickInvestingTab();
      const list = await Rates.ratesInvesingResList;
      await list[0].click();
      await browser.switchWindow(Rates.linkToTdmeritade);
      await expect(browser).toHaveUrl(Rates.linkToTdmeritade);
    });

    it('Close the new opened window and click on mortgage tab', async function () {
      await browser.closeWindow();
      await browser.switchWindow(Rates.linkFinanceYahooRates);
      const frame = await FinanceIndexPage.iFrameTo30YearFixed;
      await browser.switchToFrame(frame);
      await Mortgage.clickMortgageTab();
    });
    after(async function () {
      await FinanceIndexPage.close();
    });
  });
});
