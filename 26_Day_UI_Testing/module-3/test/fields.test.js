import FinanceIndexPage from '../page_objects/FinanceIndexPage';
import Rates from '../page_objects/Rates';
import Mortgage from '../page_objects/Mortgage';
/**#### Fields initialization, results verifications
1. Open https://finance.yahoo.com/rates using main menu
1. Click at Mortgage tab
1. Click on Purchase button
1. Type value "10001" for Zip Code input
1. Type value "1000000" for Property Value input
1. Type value "25" for Property Value input
1. Click at More Options link
1. Enable "Include FHA loan options" checkbox
1. Verify that the result table contains at least one item
1. Verify that APR cell contains percent value
1. Verify that APR cell contains date
1. Verify that Rate cell contains percent value
1. Verify that Rate cell contains points flat value with "Points" prefix
1. Verify that Mo. Payment cell contains monetary value
1. Verify that Mo. Payment cell contains monetary value with "Fees" prefix */
describe('As a Yahoo Finance site user', function () {
  before(async function () {
    await FinanceIndexPage.open('/');
  });
  describe('Fields initialization, results verifications', function () {
    it('Click at Mortgage tab', async function () {
      await FinanceIndexPage.openRates();
      const frame = await FinanceIndexPage.iFrameTo30YearFixed;
      await browser.switchToFrame(frame);
      await Rates.clickMoreBtn();
      await Mortgage.clickMortgageTab();
    });

    it('Click on Purchase button', async function () {
      await Mortgage.clickPurchaseBtn();
    });

    it('Type value "10001" for Zip Code input', async function () {
      await Mortgage.setValueZipField('10001');
    });

    it('Type value "1000000" for Property Value input', async function () {
      await Mortgage.clickPropertyValueField();
      await Mortgage.clearElementValue('#input-111');
      await Mortgage.setValuePropertyValueField('1000000');
    });

    it('Type value "25" for Property Value input', async function () {
      await Mortgage.clearElementValue('#input-111');
      await Mortgage.setValuePropertyValueField('25');
    });

    it('click more options', async function () {
      await Mortgage.clickMoreOptions();
    });

    it('Click FHA checkbox', async function () {
      await Mortgage.clickOnElement(Mortgage.checkboxFHALoan);
    });

    it('Does the list contain the results', async function () {
      const value = await Mortgage.resultsList;
      await expect(value).toBeDisplayed();
    });
  });
  after(async function () {
    await Rates.close();
  });
});
