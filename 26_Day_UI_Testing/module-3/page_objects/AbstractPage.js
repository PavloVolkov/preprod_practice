class AbstractPage {
  async open(pageUrl) {
    await browser.url(pageUrl);
    await this.waitForPageLoading();
  }

  async waitForPageLoading() {
    await browser.waitUntil(
      async () => {
        const state = await browser.execute('return document.readyState;');
        return state === 'complete';
      },
      {
        timeout: 10000,
        timeoutMsg: 'Page loading timeout',
        interval: 1000,
      },
    );
  }
  async pause(ms) {
    await browser.pause(ms);
  }

  async scrollDown(pixels) {
    await browser.executeScript(`window.scrollBy(0,${pixels})`, []);
  }

  async close() {
    await browser.closeWindow();
  }
}

export default AbstractPage;
