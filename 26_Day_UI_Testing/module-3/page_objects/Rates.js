import AbstractPage from './AbstractPage';

class Rates extends AbstractPage {
  get moreBtn() {
    return $('//span[contains(text(),"More")]');
  }

  get investingTab() {
    return $('//a[contains(text(),"Investing")]');
  }

  get paginationButtonNextPage() {
    return $('//span[contains(text(),"of ")]//parent::div//button[not(contains(@disabled, "disabled"))]');
  }

  get ditailsList() {
    return $('//*[@id="investing"]//ul').$$('li');
  }

  get ditails() {
    return $('(//*[@id="investing"]//div[contains(text(), "Expand details")])[last()]');
  }

  get learnMoreBtn() {
    return $('//*[@id="investing"]//span[contains(text(), "Learn More")]');
  }

  get investmentsTypeList() {
    return $('//label[contains(text(), "Investment type")]//parent::div');
  }

  get investmentsTypeListElements() {
    return $('#list-90').$$('div');
  }

  get ratesInvesingResList() {
    return $$("//*[@id='investing']/div/div[3]/div[4]//a[contains(@href,'https://www.myfinance.com/reporting/')]/span");
  }

  get linkToTdmeritade() {
    return 'https://www.tdameritrade.com/tools-and-platforms/trader-offering.html';
  }

  get linkFinanceYahooRates() {
    return 'https://finance.yahoo.com/rates/';
  }

  async clickInvestmentsTypeList() {
    await this.investmentsTypeList.click();
  }

  async expandDitails() {
    await this.ditails.click();
  }

  async clickLearnMoreBtn() {
    await this.learnMoreBtn.click();
  }

  async clickPaginationButtonNextPage() {
    await this.paginationButtonNextPage.click();
  }

  async clickPaginationButtonNextPage() {
    await this.paginationButtonNextPage.click();
  }

  async clickMoreBtn() {
    await this.moreBtn.click();
  }

  async clickInvestingTab() {
    await this.investingTab.click();
  }
}

module.exports = new Rates();
