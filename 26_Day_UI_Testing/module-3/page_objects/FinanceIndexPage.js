import AbstractPage from './AbstractPage';

class FinanceIndexPage extends AbstractPage {
  get personalFinanceMenuItem() {
    return $('//*/a[text()="Personal Finance"]');
  }

  get iFrameTo30YearFixed() {
    return $("//iframe[@title='Mortgage Rates for 30 year fixed']");
  }

  get rates_personalFinanceMenuItem() {
    return $('//*/a[text()="Rates"]');
  }

  async clickRates_personalFinanceMenuItem() {
    await this.rates_personalFinanceMenuItem.click();
  }

  async openInvesmentTypeList() {
    await this.invesmentTypeList.click();
  }

  async switchFrame() {
    await browser.switchToFrame(this.iFrameTo30YearFixed);
  }

  async openRates() {
    await this.hoverFinanceMenuItem();
    await this.clickRates_personalFinanceMenuItem();
  }

  async hoverFinanceMenuItem() {
    await this.personalFinanceMenuItem.moveTo();
  }
}
module.exports = new FinanceIndexPage();
