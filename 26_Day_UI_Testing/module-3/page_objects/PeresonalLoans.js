import AbstractPage from './AbstractPage';

class PersonalLoans extends AbstractPage {
  get personalLoansTab() {
    return $('//*/a[@href="#personal_loans"]');
  }
  get sliderLoanAmount() {
    return $('//*[@id="personal_loans"]//div[contains(@class, "v-slider__thumb")]//child::div');
  }
  get resultListWrapper() {
    return $(
      '//*[@id="personal_loans"]//div[contains(@role, "progressbar")]//following-sibling::div[contains(@class, "container ")]',
    );
  }
  get loansResultList() {
    return $$("//div[contains(@class, 'py-0')][3]//div[contains(@class, 'mf-product__num')]");
  }
  async clickPersonalLoansTab() {
    await this.personalLoansTab.click();
  }
  async sliderDrag(coordinates) {
    await browser.pause(1000);
    await this.sliderLoanAmount.dragAndDrop(coordinates);
  }
}

module.exports = new PersonalLoans();
