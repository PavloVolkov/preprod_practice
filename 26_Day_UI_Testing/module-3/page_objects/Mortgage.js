import AbstractPage from './AbstractPage';

class Mortgage extends AbstractPage {
  get mortgageTab() {
    return $('//*[@id="app"]//span[contains(text(),"More")]');
  }
  get checkboxFHALoan() {
    return $('//label[contains(text(),"Include FHA loan options")]//preceding-sibling::div');
  }
  get purchaseBtn() {
    return $('//span[contains(text(),"Purchase")]//parent::button');
  }
  get resultsList() {
    return $('//*/h3[text()="No Results Found"]');
  }
  get zipField() {
    return $('//*[@id="input-104"]');
  }
  get textMarkerZipField() {
    return $('//*[@id="mortgage"]//span[contains(@class, "city-state-append")]');
  }
  get zipInputWrapper() {
    return $('//*[@id="mortgage"]//div[contains(@class, "zipcode-input")]');
  }
  get moreOptions() {
    return $('//*[@id="mortgage"]//span[contains(@class, "mf-product__subtitle")]');
  }
  get propertyValueField() {
    return $('#input-111');
  }
  get clearInputCustom() {
    return selector => {
      const el = document.querySelector(selector);
      el.value = '';
    };
  }
  async setValuePropertyValueField(value) {
    return await this.propertyValueField.setValue(value);
  }
  async clickPropertyValueField() {
    return await this.propertyValueField.click();
  }
  async clearElementValue(value) {
    return await browser.execute(this.clearInputCustom, value);
  }
  async clearPropertyValueField() {
    return await this.propertyValueField.clearValue();
  }
  async clickMoreOptions() {
    await this.moreOptions.click();
  }
  async setValueZipField(value) {
    return await this.zipField.setValue(value);
  }
  async getTextFromElement(element) {
    return await element.getText();
  }
  async clickPurchaseBtn() {
    await this.purchaseBtn.click();
  }
  async clickcheckboxFHALoan() {
    await this.checkbox.click();
  }
  async clickOnElement(element) {
    await element.click();
  }
  async clickMortgageTab() {
    await this.mortgageTab.click();
  }
}
module.exports = new Mortgage();
