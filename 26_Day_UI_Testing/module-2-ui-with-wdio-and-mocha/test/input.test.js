import FinanceIndexPage from '../page_objects/FinanceIndexPage';
import { expect } from 'chai';

describe('As a Yahoo Finance site user', function () {
  describe('I should have the opportunity to use search features so that', function () {
    before(async function () {
      await FinanceIndexPage.open();
    });

    it('search input field is displayed', async function () {
      // TODO: Implement test scenario
      const inputSearchElement = await FinanceIndexPage.isInputSearchDisplayed();
      expect(inputSearchElement).to.be.true;
    });

    it('search input field is enabled', async function () {
      const inputSearchElement = await FinanceIndexPage.isInputSearchEnabled();
      expect(inputSearchElement).to.be.true;
    });

    it('search input field is clickable', async function () {
      const inputSearchElement = await FinanceIndexPage.isInputSearchClickable();
      expect(inputSearchElement).to.be.true;
    });

    it('search button is displayed', async function () {
      // TODO: Implement test scenario
      const searchButton = await FinanceIndexPage.isSearchButtonDisplayed();
      expect(searchButton).to.be.true;
    });

    it('search button is clickable', async function () {
      const searchButton = await FinanceIndexPage.isSearchButtonClickable();
      expect(searchButton).to.be.true;
    });

    after(async function () {
      await FinanceIndexPage.close();
    });
  });
});
