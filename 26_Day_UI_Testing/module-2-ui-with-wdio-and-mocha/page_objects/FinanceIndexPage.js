import AbstractPage from './AbstractPage';

class FinanceIndexPage extends AbstractPage {
  async open() {
    await super.open('/');
  }

  get futureCarouselBlock() {
    return $('div#YDC-Lead');
  }

  get videoListBlock() {
    // TODO: put getting the element using CSS selector for block marked with 2 on screenshot 1
    return $('div#Col1-0-ThreeAmigos-Proxy div[data-test="three-amigos-lucky"] > div > ul');
  }

  get newsArticleListBlock() {
    // TODO: put getting the element using CSS selector for block marked with 3 on screenshot 1
    return $('div#slingstoneStream-0-Stream');
  }

  get cryptocurrenciesListBlock() {
    // TODO: put getting the element using CSS selector for block marked with 4 on screenshot 1
    return $('#data-util-col [data-yaft-module="tdv2-applet-crypto_currencies"]');
  }

  get inputSearch() {
    return $('#yfin-usr-qry');
  }

  get searchButton() {
    return $('#header-desktop-search-button');
  }

  async menuItemElement(menuItemName) {
    // TODO: put getting the element using XPath selector for elements marked with 5 on screenshot 1
    return await $(`//*/a[text()='${menuItemName}']`);
  }

  async isFutureCarouselBlockDisplayed() {
    return await this.futureCarouselBlock.isDisplayed();
  }

  async isVideoListBlockDisplayed() {
    return await this.videoListBlock.isDisplayed();
  }

  async isNewsArticleListBlockDisplayed() {
    return await this.newsArticleListBlock.isDisplayed();
  }

  async isCryptocurrenciesListBlockDisplayed() {
    return await this.cryptocurrenciesListBlock.isDisplayed();
  }

  async isMenuItemElementDisplayed(menuItemName) {
    return await this.menuItemElement(menuItemName).isDisplayed();
  }

  async isInputSearchDisplayed() {
    return await this.inputSearch.isDisplayed();
  }

  async isInputSearchEnabled() {
    return await this.inputSearch.isEnabled();
  }

  async isInputSearchClickable() {
    return await this.inputSearch.isClickable();
  }

  async isSearchButtonDisplayed() {
    return await this.searchButton.isDisplayed();
  }

  async isSearchButtonClickable() {
    return await this.searchButton.isClickable();
  }
}

module.exports = new FinanceIndexPage();
