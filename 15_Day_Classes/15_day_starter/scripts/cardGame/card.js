/*
Card Class Description:
Class members:

properties:

suit : the suit of the card (i.e. ['Hearts', 'Diamonds', 'Clubs', ‘Spades']).
rank: an integer from 1 to 13. ("Ace" is 1, "King" is 13, i.e. {1: 'Ace', 11: 'Jack', 12: 'Queen', 13: 'King'}).
isFaceCard : a readonly property that defines whether a card is a face card (i.e. rank is either 1 or > 10).


methods:

toString(): human-readable string representation of the card (e.g. "Ace of Spades", "10 of Clubs", "Queen of Hearts" etc.)
Compare(cardOne, cardTwo): Cards must be Comparable to other cards by ranks only (no special handling for Ace).
*/
class Card {
    compare(cardOne, cardTwo){
      if (cardOne[0][2] > cardTwo[0][2]) {
        return 1
      }
      if (cardOne[0][2] < cardTwo[0][2]) {
        return 2
      }
      if (cardOne[0][2] === cardTwo[0][2]) {
        return 0
      }
    }
    toString(cardElArr){
      return `${cardElArr[0]} of ${cardElArr[1]}`
    };
  }

  export {Card}