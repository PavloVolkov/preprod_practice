import { Deck } from "./deck.js";

/*
Player Class Description
Class members:

properties:

name : player name;
wins: number of wins, readonly;
deck : a deck of cards;


methods:

Play(player1, player2): starts the game;



Implementation Details:

Players both take a card from their deck.
Whoever has a card with higher value wins the round and gets one point (if the cards are of the same value, neither of them gets a point).
After the two cards are discarded (removed from the deck), they flip the next card from the deck.
The game lasts until they are run out of cards.
When game ends, figure out who is a winner (compare their scores) and return a summing up message with the final score: {Winner} wins {X} to {Y} (i.e. "John wins 10 to 7").
*/
  
class Player {
    constructor(name) {
      this.name = name,
      this.wins = 0;
    }
    #win() {
      return this.wins++
      }
    static play(player1, player2, deck){
      if(deck.cards.length <= 0) {
        if (player1.wins === player2.wins) {
          return console.log('its a drow')
        }
         console.log(`the game is end deck is empty`)
         console.log(`${player1.wins > player2.wins ? player1.name : player2.name} is a winner,
        ${player1.wins > player2.wins ? `${player1.wins} to ${player2.wins}` : `${player2.wins} to ${player1.wins}`}`)
        return
        }
      if(deck.cards.length !== 0) {
        deck.shuffle()
        let p1_card = deck.draw(1)
        let p2_card = deck.draw(1)
        const winner = deck.compare(p1_card, p2_card)
        if(winner === 1) {
          player1.#win()
          console.log(`${player1.name} has ${deck.toString(p1_card[0])} and ${player2.name} has ${deck.toString(p2_card[0])}`)
          console.log(`${player1.name} winner`)
        }
        if(winner === 2) {
          player2.#win()
          console.log(`${player1.name} has ${deck.toString(p1_card[0])} and ${player2.name} has ${deck.toString(p2_card[0])}`)
          console.log(`${player2.name} winner`)}
      }
      return this.play(player1, player2, deck)
    }
  }

// const player1 = new Player('kris')
// const player2 = new Player('mark')

// Player.play(player1, player2, new Deck())
