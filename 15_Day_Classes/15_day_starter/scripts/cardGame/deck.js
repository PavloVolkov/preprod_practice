import { Card } from "./card.js";

/*
Deck Class Description:
Class members:

properties:

cards : contains an array of remaining cards in the deck (initially it is 52).
count : number of remaining cards in the deck, should be readonly.


methods:

shuffle() : rearranges cards in the deck randomly.
draw(n) : removes the last n cards from the deck and returns them.



Implementation Details: Initially each deck is filled with 52 cards (13 from each of 4 suits).
*/

class Deck extends Card {
    constructor() {
      super()
      this.cards = (() =>{
        let suitsProp = ['clubs', 'diamonds', 'hearts', 'spades'];
        let cardsProp = [ 'Ace', 2,	3,	4,	5,	6,	7,	8,	9,	10,	'Jack',	'Queen',	'King'];
        let res = [];
        suitsProp.forEach(el=>{
          cardsProp.forEach((e, ind)=>{
              res.push([`${el}`, `${e}`, ind+1])
          })
        })
        return res
      })()
    }
    get count(){
      return this.cards.length
    }
    draw(n) {
      return this.cards.splice(this.cards.length - n, n)
    }
    shuffle() {
      for (let i = this.cards.length - 1; i > 0; i--) {
          let j = Math.floor(Math.random() * (i + 1));
          let temp = this.cards[i];
          this.cards[i] = this.cards[j];
          this.cards[j] = temp;
      }
    }
  }

export {Deck}