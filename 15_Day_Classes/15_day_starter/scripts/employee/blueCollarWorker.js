import { Employee } from './employee.js';

/*
BlueCollarWorker Class Description:
Implementation Details: Inherits from Employee, no special logic applied here.
*/

class BlueCollarWorker extends Employee{
    constructor({firstName, lastName, birthday, salary, position, department}){
      super()
    }
  }

export {BlueCollarWorker}