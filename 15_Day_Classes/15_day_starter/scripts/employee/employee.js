/*
You'll need to implement inheritance in JS: a base class Employee that takes a single data object, two derived from it classes: Manager and BlueCollarWorker, and two more classes that inherit from Manager: HRManager and SalesManager.

Employee Class Description:
Class members:

properties:

id ;
firstName ;
lastName ;
birthday ;
salary ;
position ;
department ;
age : readonly property dynamically calculated based on birthday;
fullName: readonly;
EMPLOYEES : static readonly property where each user gets registered on initialization (contains list of all instances, except intentionally deleted once).


methods:

quit() - remove the employee from EMPLOYEES;
retire() - log a message: "It was such a pleasure to work with you!" and remove from EMPLOYEES;
getFired() - log a message: "Not a big deal!" and remove from EMPLOYEES;
changeDepartment(newDepartment) ;
changePosition(newPosition) ;
changeSalary(newSalary) ;
getPromoted(benefits) - takes an object that can contain salary, position and department in any combination. If the property is defined, then appropriate method is called. Log: "Yoohooo!"
getDemoted(punishment) - apply the same logic as to getPromoted. Log: "Damn!"
*/
class Employee {
        static EMPLOYEES = []
        static ID = 1
        constructor({firstName, lastName, birthday, salary, position, department}){
        this.id = (() => {
            const S4 = function() {
               return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
            };
            const posibleId = (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4()) 
            const isIdUnique = Employee.EMPLOYEES.find(el=>el.id === posibleId)
            if (isIdUnique === undefined) {
                return posibleId                  
            }
            return this.id
        })()
        this.firstName = firstName,
        this.lastName = lastName,
        this.birthday = birthday,
        this.salary = salary,
        this.position = position,
        this.department = department,
        Employee.EMPLOYEES.push(this)
        // Employee.ID++
        }
    quit(){
        const toDelete = Employee.EMPLOYEES.find((e)=> e.id === this.id)
        const indexDel = Employee.EMPLOYEES.indexOf(toDelete)
        if (indexDel === -1){
        return console.log(`this user already deleted`)
        }
        Employee.EMPLOYEES.splice(indexDel, 1)
        console.log(`user with id ${this.id} deleted`)
    }
    get age(){
        const birthday = new Date(this.birthday);
        const today_date = new Date();
        const today_year = today_date.getFullYear();
        const today_month = today_date.getMonth();
        const today_day = today_date.getDate();
        let age = today_year - birthday.getFullYear();
        if(today_month <= birthday.getMonth() && today_day < birthday.getDate()) {
            age-=1
            return age 
            }
        if (today_month + 1 < birthday.getMonth()){
            age-=1
            return age
        }
        return age
    }
    get fullName(){
        return `${this.firstName} ${this.lastName}`
    }
    retire(){
        this.quit()
        return console.log('It was such a pleasure to work with you!')
    }
    getFired(){
        this.quit()
        return console.log('Not a big deal!')
    }
    changeDepartment(newDepartment){
        return this.department = newDepartment
    }
    changePosition(newPosition){
        return this.posititon = newPosition
    }
    changeSalary(newSalary){
        return this.salary = newSalary
    }
    getPromoted({salary, position, department}){
        this.salary = salary
        this.position = position
        this.department = department
        console.log('Yoohooo!')
    }
    getDemoted({salary, position, department}){
        this.salary = salary
        this.position = position
        this.department = department
        console.log('Damn!')
    }
}

export {Employee}