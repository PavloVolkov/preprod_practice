import { Manager } from './manager.js';

/*
SalesManager Class Implementation:
Implementation Details: Inherits from Manager. Its constructor does not require department property, it should always be initialized as ‘sales’.
*/

class SalesManager extends Manager {
  constructor({ firstName, lastName, birthday, salary, position, department }) {
    super({ firstName, lastName, birthday, salary, position });
    this.department = 'sales';
  }
}
