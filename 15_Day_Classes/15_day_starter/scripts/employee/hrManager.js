import { Manager } from './manager.js';

/*
HRManager Class Description:
Implementation Details: Inherits from Manager. Its constructor does not require department property, it should always be initialized as ‘hr’.
*/

class HRManager extends Manager{
    constructor({firstName, lastName, birthday, salary, position, department}){
      super({firstName, lastName, birthday, salary, position})
      this.department = 'hr'
    }
  }

