import { Employee } from './employee.js';

/*
Manager Class Description:
Implementation Details: Inherits from Employee. Its constructor does not require position property, it should always be initialized as ‘manager’.
Class members:

properties:

managedEmployees : readonly, selects all instances that belong to his/her department that are not managers.
*/
class Manager extends Employee{
    constructor({firstName, lastName, birthday, salary, position, department}){
      super({firstName, lastName, birthday, salary, department})
      this.position = 'manager',
      this.managedEmployees = [],
      this.managedEmployees = Employee.EMPLOYEES.filter(el=>{
        return el.position !== this.position && el.department === this.department
        })
    }
  }

export {Manager}