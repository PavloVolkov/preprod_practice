/*
Create a class called PersonAccount. It has firstname, lastname, incomes, 
expenses properties and it has totalIncome, totalExpense, accountInfo, addIncome, 
addExpense and accountBalance methods. Incomes is a set of incomes and its 
description and expenses is also a set of expenses and its description.
*/

class PersonAccount {
    constructor(firstname, lastname){
      this.firstname = firstname,
      this.lastname = lastname,
      this.incomes = [],
      this.expenses = [];
    }
    set addIncomes(income) {
      return this.incomes.push(income)
    }
    set addExpences(expense) {
      return this.expenses.push(expense)
    }
    get getIncomes() {
      return this.incomes
    }
    get getExpenses() {
      return this.expenses
    }
    totalIncome(){
      return this.incomes.reduce((acc, {sum})=>{
        return acc += sum
      }, 0)
    }
    totalExpense(){
      return this.expenses.reduce((acc, {sum})=>{
        return acc += sum
      }, 0)
    }
    accountBalance(){
       return this.totalIncome() - this.totalExpense()
    }
    accountInfo(){
      return `User ${this.firstname} ${this.lastname} has balance: ${this.accountBalance()}$`
    }
  }
  
  let user = new PersonAccount('john', 'doe')
  
//   user.addIncomes = {'description': 'new income1', 'sum': 100};
//   user.addIncomes = {'description': 'new income2', 'sum': 150};
//   user.addIncomes = {'description': 'new income3', 'sum': 175};
//   user.addIncomes = { 'description': 'new income4', 'sum': 250};
  
//   user.addExpences = {'description': 'new expence1', 'sum': 100};
//   user.addExpences = {'description': 'new expence2', 'sum': 150};
//   user.addExpences = {'description': 'new expence3', 'sum': 175};
  
//   console.log(user.getIncomes)
//   console.log(user.getExpenses)
//   console.log(user.totalIncome())
//   console.log(user.totalExpense())
//   console.log(user.accountBalance())
//   console.log(user.accountInfo())
//   console.log(user.getIncomes)
//   console.log(user.getExpenses)