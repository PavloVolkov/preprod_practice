// 1. Create an Animal class. 
//The class will have name, age, color, legs properties and create different methods
class Animal {
  constructor (name, age, color, legs) {
    this.name = name;
    this.age = age;
    this.color = color;
    this.legs = legs;
    this.skills = [];
  }
  get animalName() {
    return this.name
  }
  set rename(name) {
    this.name = name
  }
  aboutAnimal(){
    return `The ${this.name} is a cute pet, it's ${this.age} yars old, has ${this.color} fur and ${this.legs} legs`
  }
}
// let pet = new Animal('dogo', 5, 'brown', 4);
// console.log(pet.name)
// console.log(pet.animalName)
// pet.rename = 'crispy'
// console.log(pet.animalName)
// console.log(pet.aboutAnimal())

// 2. Create a Dog and Cat child class from the Animal Class.
// 3. Override the method you create in Animal class
class Dog extends Animal {
  aboutAnimal(){
    return `The ${this.name} is the happiest dog, it is forever yang, has ${this.color} fur and ${this.legs} legs`
  }
}
// const dog1 = new Dog('lucky', 2, 'black', 4)
// console.log(dog1.aboutAnimal())
class Cat extends Animal {
  aboutAnimal(){
    return `The ${this.name} is an angry cat, it has ${this.age} wisdom, has ${this.color} fur and ${this.legs} legs`
  }
}
// const cat1 = new Cat('morpheus', 'less 1 year', 'brown-milk', 4)
// console.log(cat1.aboutAnimal())