import AbstractPage from './AbstractPage';

class HomePage extends AbstractPage {
  constructor() {
    super('Home');
  }

  async open() {
    await super.open('/');
  }

  get menuNavItems() {
    return $$('li[data-subnav-type] a.nr-applet-nav-item');
  }

  menuItemElement(menuItemName) {
    return $(`.//a[contains(@class, "nr-applet-nav-item")][@title="${menuItemName}"]`);
  }

  async getMenuNavItemsText(menuItemNames) {
    const displayedElements = [];
    for (const menuItemName of menuItemNames) {
      const element = await $(`//a[contains(text(),"${menuItemName}")]`).getText();
      displayedElements.push(await element);
    }
    // const displayedElements = await menuItemNames.map(menuItemName => {
    //   const element = $(`//a[contains(text(),"${menuItemName}")]`);
    //   return element.getText();
    // });
    return await displayedElements;
  }

  async clickOnMenuItem(menuItemName) {
    await this.click(await this.menuItemElement(menuItemName));
    await this.waitForPageLoading();
  }
}

module.exports = HomePage;
