import { expect } from 'chai';
import { When, Then } from '@wdio/cucumber-framework';

import PageFactory from '../page_objects/PageFactory';

Then(/^the "([^"]*)" button is displayed$/, async button => {
  const page = await PageFactory.getPage('Screener');

  let isButtonDisplayed = null;

  switch (button) {
    case 'Create New Screener':
      isButtonDisplayed = await page.isCreateNewScreenButtonDisplayed();
      expect(isButtonDisplayed).to.be.true;
      break;
    default:
      throw new Error(`Option ${button} is not available`);
  }
});

Then(/^the "([^"]*)" button is clickable$/, async button => {
  const page = await PageFactory.getPage('Screener');

  let isButtonClickable = null;

  switch (button) {
    case 'Create New Screener':
      isButtonClickable = await page.isCreateNewScreenButtonDisplayed();
      expect(isButtonClickable).to.be.true;
      break;
    default:
      throw new Error(`Option ${button} is not available`);
  }
});

When(/^the user clicks on the "([^"]*)" button$/, async button => {
  // TODO: Implement a step that clicks the button according to the given argument - "button"
  const page = await PageFactory.getPage('Screener');

  switch (button) {
    case 'Create New Screener':
      await page.createNewScreenButton.click();
      break;
    case 'Equity Screener':
      await page.equityScreenerButton.click();
      await browser.pause(1000);
      break;
    default:
      throw new Error(`Option ${button} is not available`);
  }
});
