import { expect } from 'chai';
import { Given, Then } from '@wdio/cucumber-framework';

import PageFactory from '../page_objects/PageFactory';

Given(/^the user is on the (\w+) page$/, async pageName => {
  const page = await PageFactory.getPage(pageName);
  await page.open();
});
Then(/^the browser URL contains "([^"]*)" on the (\w+) page$/, async (url, pageName) => {
  // TODO: Implement a step that checks the browser url according to the given arguments - "url" and "pageName"
  const page = await PageFactory.getPage(pageName);
  const currentUrl = await page.getUrl();
  expect(currentUrl).to.have.string(url);
});

Then(/^the page title is "([^"]*)" on the "([^"]*)" page$/, async (title, pageName) => {
  const page = await PageFactory.getPage(pageName);
  const pageTitleObj = await page.getTitle();
  const pageTitleStr = await pageTitleObj.toString();
  expect(pageTitleStr).to.eql(title);
});
